package src.model;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Year;

/**
 * DaySchedule class contains an arrival time and a time of departure from company to work. A DaySchedule represents one day and here each day of the week has the same schedule.
 */
public class DaySchedule implements Serializable {
	
	/*
	 * Attributes
	 */
	
	/**
	 * serialVersionUID allows to check the success of serialization.
	 */
	private static final long serialVersionUID = 4912672954276242695L;
    /**
     * standardArrivalTime is arrival time for one day without problem.
     */
    private LocalTime standardArrivalTime;
    /**
     * standardDepartureTime is departure time for one day without problem.
     */
    private LocalTime standardDepartureTime;
    
    /*
     * Constructor
     */
    
    /**
     * Constructor
     * @param arrivalTime 
     * @param deparetureTime
     */
    public DaySchedule(LocalTime arrivalTime, LocalTime departureTime) {
    	LocalDate standardDate = LocalDate.of(Year.MIN_VALUE, 1, 1);
    	
    	LocalDateTime ldtArrival = Pointing.round(arrivalTime.atDate(standardDate));
    	LocalDateTime ldtDeparture = Pointing.round(departureTime.atDate(standardDate));
    	
    	standardArrivalTime = ldtArrival.toLocalTime();
    	standardDepartureTime = ldtDeparture.toLocalTime();
    }

    /*
     * Methods
     */
    
    // setters
    
    /**
     * This method allows to edit standardArrivalTime attribute.
     * @param newTime
     */
    public void setStandardArrivalTime(LocalTime newTime) {
    	LocalDate standardDate = LocalDate.of(Year.MIN_VALUE, 1, 1);
    	
    	LocalDateTime ldtArrival = Pointing.round(newTime.atDate(standardDate));
    	
    	standardArrivalTime = ldtArrival.toLocalTime();
    }
    /**
     * This method allows to edit standardDepartureTime attribute.
     * @param newTime
     */
    public void setStandardDepartureTime(LocalTime newTime) {
    	LocalDate standardDate = LocalDate.of(Year.MIN_VALUE, 1, 1);
    	
    	LocalDateTime ldtDeparture = Pointing.round(newTime.atDate(standardDate));
    	
    	standardDepartureTime = ldtDeparture.toLocalTime();
    }
    
    //getters
    
    /** 
     * This method returns standardArrivalTime attribute.
     * @return standardArrivalTime
     */
    public LocalTime getStandardArrivalTime() {
    	return standardArrivalTime;
    }
    
    /** 
     * This method returns standardDepartureTime attribute.
     * @return standardDepartureTime
     */
    public LocalTime getStandardDepartureTime() {
    	return standardDepartureTime;
    }
    
    //other method
    
    /**
     *  This method converts a DaySchedule object to a String.
     * @return String which represents arrival time and departure time
     */
    @Override
    public String toString() {
    	return "" + standardArrivalTime + " " + standardDepartureTime;
    }
}