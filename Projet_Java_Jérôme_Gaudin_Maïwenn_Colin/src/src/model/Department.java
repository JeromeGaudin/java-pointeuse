package src.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * @class Department class contains information about one department in the company.
 */
public class Department implements Externalizable, Comparable<Department>  {

	/**
     * Attributes
     */
	
    /**
     * id is computed thanks to idCount. id is unique, it is a login.
     */
    private int id;
    /**
     * name is department name.
     */
    private String name;
    /**
     * idCount allows to  compute an unique login (id) for each department.
     */
    private static int idCount=0;

    /*
     * Constructors
     */
    
    /**
     * Default constructor
     */
    public Department() {
    	id = idCount;
    	idCount++;
    	name = "";
    }
    /**
     * Constructor
     * @param name0
     */
    public Department(String name0) {
    	id = idCount;
    	idCount++;
    	name = name0;
    }
    
    /*
     * Methods
     */
    
    //setter
    
    /**
     * This method allows to edit name attribute.
     * @param name0
     */
    public void setName(String name0) {
        name = name0;
    }
    
    //getters
    
    /**
     * This method returns name attribute.
     * @return name
     */
    public String getName(){
    	return name;
    }
    
    /**
     * This method returns id attribute.
     * @return id
     */
    public int getId (){
    	return id;
    }
    
    /**
     * This method returns idCount attribute.
     * @return idCount
     */
    public int getIdCount (){
    	return idCount;
    }
    
    //other methods
    
    /**
     * This method converts a Person object to a String.
     * @return String  which represents department's name
     */
    @Override
    public String toString() {
    	return name;
    }
    /**
     * This method overshadows the method to serialize.
     * @param out
     * @throws IOException
     */
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(id);
		out.writeObject(name);
	}
	 /**
     * This method overshadows the method to deserialize.
     * @param in
     * @throws IOException, ClassNotFoundException
     */
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		id = in.readInt();
		name = (String) in.readObject();
		// pour ne pas retomber sur des id supprimes et creer par la suite des erreurs
		if (id > idCount) {
			idCount = id;
		}
	}
	
	/**
	 * This method allows to compare two object Department.
	 * @param Department
	 * @return int
	 */
	@Override
	public int compareTo(Department dep) {
		return id - dep.getId();
	}
	
	/**
	 * This method allows to compare two Department objects.
	 * @param Object
	 * @return boolean  
	 */
	@Override
	public boolean equals(Object o) {
		boolean res = false;
		if (o != null && o.getClass() == Department.class) {
			Department dep = (Department) o;
			if (dep.getId() == id && dep.getName() != null && dep.getName().equals(name)) {
				res = true;
			}
		}
		return res;
	}
}