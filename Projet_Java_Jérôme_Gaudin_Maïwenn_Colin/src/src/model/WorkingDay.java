package src.model;

import java.io.Serializable;
//TERMINE

/**
 * 
 */
public class WorkingDay implements Comparable<WorkingDay> , Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6743882834006264869L;
	
	/**
	 * Attributes
	 */
	/**
	 * 
	 */
	private Pointing arrivalPointing;
	/**
	 * 
	 */
	private Pointing departurePointing;
	
	/**
     * Constructor
     * @param arrvivalP
     */
    public WorkingDay(Pointing pointing) {
    	arrivalPointing = pointing;
    	departurePointing = null;
    }
    /**
     * Methods
     */
    /**
     * @param departureP
     */
    public void endWorkingDay(Pointing departureP) {
    	departurePointing = departureP;
    }
    
    //get
    public Pointing getArrival(){
    	return arrivalPointing;
    }
    
    public Pointing getDeparture(){
    	return departurePointing;
    }
    /*
    public static void main(String[] a){
    	LocalTime arr = LocalTime.now();
    	try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	LocalTime dep = LocalTime.now();
    	long b = ChronoUnit.SECONDS.between(dep,arr);
    	System.out.println(b);
    	System.out.println(arr);
    	System.out.println(dep);
    }*/

    
	@Override
	public int compareTo(WorkingDay o) {
		return arrivalPointing.getTime().compareTo(o.arrivalPointing.getTime());
	}
}