package src.model;

import java.io.Serializable;

/**
 * Manager class contains a boolean which indicates if the manager manage his department or not. A manager is an employee.
 */
public class Manager extends Employee implements Serializable {

    /*
     *Attributes 
     */
	
	/**
	 * serialVersionUID allows to check the success of serialization.
	 */
	private static final long serialVersionUID = 5593608104994831865L;
    /**
     * manage is true if the manager manage his department.
     */
    private boolean manage;
    
    /*
     *Constructors 
     */

	/**
     * Default constructor
     */
    public Manager() {
    	super();
    	manage = false;
    }
    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param department
     * @param schedule
     * @param isManage
     */
    public Manager(String firstName, String lastName, Department department, DaySchedule schedule, boolean isManage) {
    	super(firstName, lastName, department, schedule);
    	manage = isManage;
    }
    
    //setter
    
    /**
     * This method allows to edit manage attribute.
     * @param newManage
     */
    public void setManage(boolean newManage) {
    	manage = newManage;
    }
    
    //getter
    
    /** 
     * This method returns manage attribute.
     * @return manage
     */
    public boolean getManage() {
    	return manage;
    }
	
	/**
	 * This method allows to compare two Manager objects.
	 * @param o
	 * @return boolean  
	 */
	@Override
	public boolean equals(Object o) {
		boolean res = false;
		if (o != null && o.getClass() == Manager.class) {
			Manager man = (Manager) o;
			if (man.getId() == getId()) {
				res = true;
			}
		}
		return res;
	}
}