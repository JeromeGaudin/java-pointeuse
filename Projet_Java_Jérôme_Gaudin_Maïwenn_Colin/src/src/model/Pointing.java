package src.model;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * Pointing class contains a Person object and a LocalDateTime, when this person does a check in or a check out.
 */
public class Pointing implements Serializable, Comparable<Pointing> {

	/*
     * Attributes
     */
	
    /**
     * time is when a check was did.
     */
    private LocalDateTime time;
    /**
     * person is who did the check.
     */
    private Person person;
	/**
	 * serialVersionUID allows to check the success of serialization.
	 */
	private static final long serialVersionUID = -6443770479826101399L;
	
    /*
     * Constructors
     */
	
    /**
     * Constructor
     * @param person0
     */
    public Pointing(Person person0) {
    	person = person0;
    	time = LocalDateTime.now();
    	time = round(time);
    }
    
    /**
     * Constructor
     * This constructor allows to correct errors when someone forgets to check in or check out.
     * @param person0
     * @param time
     */
    public Pointing(Person person0, LocalDateTime time0) {
    	this(person0);
    	time = round(time0);
    }
    
    /*
     * Methods
     */
    
    //getters
    
    /**
     * This methods returns person attribute.
     * @return person
     */
    public Person getPerson(){
    	return person;
    }
    /**
     * This method returns time attribute.
     * @return time
     */
    public LocalDateTime getTime(){
    	return time;
    }
    
    //other methods
    
    /**
     * This method returns the same time rounded to the nearest quarter of an hour
     * @param LocalDateTime time0
     * @return LocalDateTime
     */
    public static LocalDateTime round(LocalDateTime time0) {
    	int n = time0.getMinute();
    	int r = n%15;
    	if (r<8){
    		time0 = time0.minusMinutes(r);
    	}
    	else{
    		if(n-r+15==60){
    			time0 = time0.minusMinutes(n);
    			time0 = time0.plusHours(1);
    		}
    		else{
    			time0 = time0.plusMinutes(15-r);
    		}
    	}
    	time0 = time0.minusSeconds(time0.getSecond());
    	time0 = time0.minusNanos(time0.getNano());
    	return time0;
    }
    /**
     * This method converts a Pointing object to a String.
     * @return String which represents person and time
     */
    @Override
    public String toString() {
    	return person.toString() + " " + time.toString();
    }
    /**
     * This method check if the Pointing object is the same as obj
     * @param obj
     * @return boolean is true when obj is a Pointing and its attributes are same.
     */
    @Override
    public boolean equals(Object obj) {
    	boolean res = false;
    	
    	if (obj.getClass() == Pointing.class) {
    		Pointing pointing = (Pointing) obj;
    		if (pointing.getPerson() != null && pointing.getPerson().getId() == person.getId()) {
    			if (time != null && time.equals(pointing.getTime())) {
    				res = true;
    			}
    		}
    	}
    	return res;
    }

    /**
     * This method allows to sort a List<Pointing>
     * @param p
     * @return int 
     */
	@Override
	public int compareTo(Pointing p) {
		if(time.compareTo(p.getTime()) == 0) {
			return person.getId() - p.getPerson().getId();
		} else {
			return time.compareTo(p.getTime());
		}
	}
}