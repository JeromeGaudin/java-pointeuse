package src.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;

//Methods :
//setters
//getters
//methods to edit (add, remove and addPointing)
//methods to research and get with an id
//other methods

/**
 * Company class contains important information for the company.
 */
public class Company implements Externalizable {
	
	//Attributes
	
	/**
	 * serialVersionUID allows to check the success of serialization.
	 */
	private static final long serialVersionUID = 7588159553402719116L;
	/**
	 * manager is a Set<Manager> which contains all managers in the company.
	 */
	private Set<Manager> manager;
	/**
	 * employee is a Set<Employee> which contains all employees in the company, without managers.
	 */
	private Set<Employee> employee;
	/**
	 *  department is a Set<Department> which contains all departments in the company.
	 */
	private Set<Department> department;
	/**
	 * lateTime represents the number of minutes after which an employee is considered late.
	 */
	private int lateTime;
	/**
	 * earlyTime represents the number of minutes before which an employee is considered left early.
	 */
	private int earlyTime;
	/**
	 * comboBoxModelDepartment contains all departments for an object JComboBox.
	 */
	private DefaultComboBoxModel comboBoxModelDepartment;
	/**
	 * comboBoxModelAllEmployee contains all employees for an object JComboBox.
	 */
	private DefaultComboBoxModel comboBoxModelAllEmployee;
	
	/**
	 * Default Constructor
	 */
	public Company() {
		manager = new HashSet<Manager>();
		employee = new HashSet<Employee>();
		department = new HashSet<Department>();
		earlyTime = 15;
		lateTime = 15;
		
    	comboBoxModelDepartment = new DefaultComboBoxModel();
    	for (Department dep : department) {
    		comboBoxModelDepartment.addElement(dep);
    	}
    	comboBoxModelAllEmployee = new DefaultComboBoxModel();
    	for (Employee emp : getAllEmployee()) {
    		comboBoxModelAllEmployee.addElement(emp);
    	}
	}
	
	//Methods
	
	//setters
	
	/**
	 * This method allows to edit lateTime.
	 * @param lateTime0
	 */
    public void setLateTime(int lateTime0) {
        lateTime = lateTime0;
    }
    /**
     * This method allows to edit earlyTime.
     * @param earlyTime0
     */
    public void setEarlyTime(int earlyTime0) {
        earlyTime = earlyTime0;
    }
	
	//getters
	
    /**
     * This method returns comboBoxModelDepartment attribute.
     * @return comboBoxModelDepartment
     */
    public DefaultComboBoxModel getComboBoxModelDepartment() {
		return comboBoxModelDepartment;
	}
    
    /**
     * This method returns comboBoxModelAllEmployee attribute.
     * @return comboBoxModelAllEmployee
     */
    public DefaultComboBoxModel getComboBoxModelAllEmployee() {
		return comboBoxModelAllEmployee;
	}
    
    /**
     * This method returns lateTime attribute.
     * @return lateTime
     */
    public int getLateTime() {
        return lateTime;
    }
    /**
     * This methods returns earlyTime attribute.
     * @return earlyTime
     */
    public int getEarlyTime() {
        return earlyTime;
    }
	/**
	 * This method returns employee attribute.
	 * @return employee
	 */
	public Set<Employee> getEmployee() {
		return employee;
	}
	/**
	 * This method returns department attribute.
	 * @return department
	 */
    public Set<Department> getDepartment() {
    	return department;
    }
    /**
     * This method returns manager attribute.
     * @return
     */
    public Set<Manager> getManager() {
    	return manager;
    }
	/**
	 * This method returns a Set<Employee> which contains all employees and all managers.
	 * @return Set<Employee>
	 */
	public Set<Employee> getAllEmployee() {
		Set<Employee> person = new HashSet<Employee>();
		for (Employee emp : employee) {
			person.add(emp);
		}
		for (Manager man : manager) {
			person.add(man);
		}
		return person;
	}
	
	//methods to edit (add remove and addPointing)
	
	/**
	 * This method allows to add an Employee in the employee attribute (set<Employee>). This method returns an error code. If newEmployee is already in employee attribute (set<Employee>) false is returned. True is returned if success.
	 * @param newEmployee
	 * @return boolean
	 */
	public boolean addEmployee(Employee newEmployee) {
		if (newEmployee != null) {
			return employee.add(newEmployee);
		} else {
			throw new NullPointerException();
		}
	}
	/**
	 * This method allows to add a Manager in the manager attribute (set<Manager>). This method returns an error code. If newManager is already in manager attribute (set<Manager>) false is returned. True is returned if success.
	 * @param newManager
	 * @return boolean
	 */
	public boolean addManager(Manager newManager) {
		if (newManager != null) {
			return manager.add(newManager);
		} else {
			throw new NullPointerException();
		}
	}
	/**
	 * This method allows to add a Department in the department attribute (set<Department>). This method returns an error code. If newDepartment is already in department attribute (set<Department>) false is returned. True is returned if success.
	 * @param newDepartment
	 * @return boolean
	 */
	public boolean addDepartment(Department newDepartment) {
		if (newDepartment != null) {
			return department.add(newDepartment);
		} else {
			throw new NullPointerException();
		}
	}
	/**
	 * This method allows to remove an Employee with an id (idEmployee) in the employee attribute (set<Employee>). This method returns an error code. If there is no employee with the login idEmployee in employee attribute (set<Employee>) false is returned. True is returned if success.
	 * @param idEmployee
	 * @return boolean
	 */
	public boolean removeEmployee(int idEmployee) {
    	Iterator<Employee> itr = employee.iterator();
    	while (itr.hasNext()) {
    		Employee emp = itr.next();
    		if (emp.getId() == idEmployee) 	{
    			itr.remove();
    			return true;
    		}
    	}
    	return false;
	}
	/**
	 * This method allows to remove a Manager with an id (idManager) in the manager attribute (set<Manager>). This method returns an error code. If there is no manager with the login idManager in manager attribute (set<Manager>) false is returned. True is returned if success.
	 * @param idManager
	 * @return boolean
	 */
	public boolean removeManager(int idManager) {
    	Iterator<Manager> itr = manager.iterator();
    	while (itr.hasNext()) {
    		Manager man = itr.next();
    		if (man.getId() == idManager) 	{
    			itr.remove();
    			return true;
    		}
    	}
    	return false;
	}
	/**
	 * This method allows to remove a Department with an id (idDepartment) in the department attribute (set<Department>). This method returns an error code. If there is no department with the login idDepartment in department attribute (set<Department>) false is returned. True is returned if success.
	 * @param idDepartment
	 * @return boolean
	 */
	public boolean removeDepartment(int idDepartment) {
    	Iterator<Department> itr = department.iterator();
    	while (itr.hasNext()) {
    		Department dep = itr.next();
    		if (dep.getId() == idDepartment) 	{
    			itr.remove();
    			return true;
    		}	
    	}
    	return false;
	}
	/**
	 * This method allows to update employee's historic for each employee with a new pointing, it don't add the pointing if it exist in historic. 
     * @param pointing
     */
    public void addPointing(Pointing pointing) {
    	for (Employee empTmp : getAllEmployee()) {
    		if (pointing.getPerson().getId() == empTmp.getId()) {
    			
    			// test if pointing exist in this historic
    			if ( !empTmp.containsPointing(pointing)) {

	    			List<WorkingDay> historic = empTmp.getHistoric();
	    			if (empTmp.getCurrentDay() == true) {
	    				WorkingDay aWorkingDay = historic.get(historic.size() - 1);
	    				aWorkingDay.endWorkingDay(pointing);
	    			} else {
	    				historic.add(new WorkingDay(pointing));
	    			}
					empTmp.setCurrentDay( !empTmp.getCurrentDay());
					empTmp.updateOverTime(pointing);
					return;
    			}
    		}
    	}
    }
	
    // methods to research and get with an id
    
    /**
     * This method returns a List<Pointing> which contains all Pointing with anomaly for arrival time. Employee concerned was late. 
     * @return List<Pointing>
     */
    public List<Pointing>showLateArrival() {
    	int minutes = lateTime;
        List<Pointing> pointingLate = new ArrayList<Pointing>();       
        
        for (Employee empTmp : getAllEmployee()) {
        	
            LocalTime T0 = empTmp.getSchedule().getStandardArrivalTime();
            
            for (WorkingDay aWorkingDay : empTmp.getHistoric()) {
            	
            	LocalTime T1 = aWorkingDay.getArrival().getTime().toLocalTime();
	            long a = ChronoUnit.MINUTES.between(T0,T1);
	            if (a >= (long) minutes){
	            	pointingLate.add(aWorkingDay.getArrival());
	            }
	        }
        }
        return pointingLate;
    }
    /**
     *  This method returns a List<Pointing> which contains all Pointing with anomaly for departure time. Employee concerned was early.
     * @return List<Pointing>
     */
    public List<Pointing> showAdvanceDeparture() {
    	int minutes = earlyTime;
    	List<Pointing> pointingAdvance = new ArrayList<Pointing>();       
    	 
        for (Employee empTmp : getAllEmployee()) {
        	
        	LocalTime T1 = empTmp.getSchedule().getStandardDepartureTime();
            
            for (WorkingDay aWorkingDay : empTmp.getHistoric()) {
	         
            	if (aWorkingDay.getDeparture() != null){
	            	LocalTime T0 = aWorkingDay.getDeparture().getTime().toLocalTime();
		            long a = ChronoUnit.MINUTES.between(T0,T1);
		            if (a >= (long) minutes){
		            	pointingAdvance.add(aWorkingDay.getDeparture());
		            }
            	}
            }
        }
        return pointingAdvance;
    }
    /**
     * This method returns a List<Employee> which contains all Employee whom their department's login is idDepartment.
     * @param idDepartment
     * @return List<Employee>
     */
    public List<Employee> getAllEmpoyeesWithDepartment(int idDepartment) {
		Department dep = getDepartment(idDepartment);
		if (dep == null) {
			return null;
		}
		
		List<Employee> empList = new ArrayList<Employee>();
		for (Employee emp : employee) {
			if (emp.getDepartment() != null && emp.getDepartment().getId() == dep.getId()) {
				empList.add(emp);
			}
		}
		for (Employee emp : manager) {
			if (emp.getDepartment() != null && emp.getDepartment().getId() == dep.getId()) {
				empList.add(emp);
			}
		}
		return empList;
	}
    /**
     * This method returns an Employee object with an id (idEmployee)
     * @param idEmployee
     * @return Employee with the same idEmployee or null if it is not found 
     */
    public Employee getEmployee(int idEmployee) {
    	Iterator<Employee> itr = employee.iterator();
    	while (itr.hasNext()) {
    		Employee emp = itr.next();
    		if (emp.getId() == idEmployee) 	{
    			return emp;
    		}
    	}
    	Iterator<Manager> itr2 = manager.iterator();
    	while (itr2.hasNext()) {
    		Employee emp = (Employee) itr2.next();
    		if (emp.getId() == idEmployee) 	{
    			return emp;
    		}
    	}
    	return null;
    }
    /**
     * This method returns a Manager object with an id (idManager)
     * @param idManager
     * @return Manager with the same idManager or null if it is not found 
     */
    public Manager getManager(int idManager) {
    	Iterator<Manager> itr = manager.iterator();
    	while (itr.hasNext()) {
    		Manager man = itr.next();
    		if (man.getId() == idManager) 	{
    			return man;
    		}
    	}
    	return null;
    }
    /**
     * This method returns a Department object with an id (idDepartment)
     * @param idDepartment
     * @return Department with the same idDepartment or null if it is not found 
     */
    public Department getDepartment(int idDepartment) {
    	Iterator<Department> itr = department.iterator();
    	while (itr.hasNext()) {
    		Department dep = itr.next();
    		if (dep.getId() == idDepartment) 	{
    			return dep;
    		}
    	}
    	return null;
    }
	/**
     * This method returns a List<Pointing> which contains all Pointing between arrival and departure, of one employee or all employees in one department
     * @param begining is the beginning of the requested period
     * @param ending is the end of the requested period
     * @param person is the wanted person
     * @param dep is the wanted department
     * @return List<Pointing>
     */
	public List<Pointing> searchPointing(LocalDateTime begining, LocalDateTime ending, Person person, Department dep) {
    	Set<Employee> employees;
    	
    	// employee
    	if (person == null) {
    		employees = getAllEmployee();
    	} else {
    		employees = new HashSet<Employee>();
    		employees.add(getEmployee(person.getId()));
    	}
    	
    	// department
    	if ( !employees.isEmpty()) {
    		if (dep != null) {
    			// on construit la liste des employés avec le bon département
    			Iterator<Employee> itr = employees.iterator();
    			while (itr.hasNext()) {
    	    		Employee emp = itr.next();
    	    		if (emp.getDepartment() == null || emp.getDepartment().getId() != dep.getId()) {
    	    			itr.remove();
    	    		}
    			}
    		}
    	}
    	
    	List<Pointing> pointings = new ArrayList<Pointing>();
    	if ( !employees.isEmpty()) {
	    	Iterator<Employee> itr = employees.iterator();
			while (itr.hasNext()) {
	    		Employee emp = itr.next();
	    		
	    		for (Pointing pointing : emp.getAllPointing()) {
	    			
	    			if (begining == null) {
	    				pointings.add(pointing);
	    			} else {
	    				if ( !pointing.getTime().isBefore(begining)) {
	    					pointings.add(pointing);
	    				}
	    			}
	    		}
			}
			if ( !pointings.isEmpty()) {
				int i = 0;
				while (i < pointings.size()) {
					// delete pointing of the list if the pointing are after the end
					if (ending != null && pointings.get(i).getTime().isAfter(ending)) {
						pointings.remove(pointings.get(i));
					} else {
						i++;
					}
				}
			}
    	}
		return pointings;
    }
	
    //other method
	/**
	 * This method allows to know if an employee is a manager too.
	 * @param idEmployee
	 * @return boolean
	 */
	public boolean isManager(int idEmployee) {
		for (Manager mana : manager) {
			if (mana.getId() == idEmployee) {
				return true;
			}
		}
		return false;
		
	}
    /**
     * This method converts a Company object to a String.
     * @return String which represents company's contents
     */
    @Override
    public String toString() {
    	return "employee: " + employee + ", manager: " + manager + ", department: " + department;
    }
    
    /**
    * This method overshadows the method to serialize.
    * @param out
    * @throws IOException
    */
	@Override
	public void writeExternal(ObjectOutput oo) throws IOException {
		oo.writeInt(earlyTime);
		oo.writeInt(lateTime);
		oo.writeObject(employee);
		oo.writeObject(manager);
		oo.writeObject(department);
	}
	
	 /**
     * This method overshadows the method to deserialize.
     * @param in
     * @throws IOException, ClassNotFoundException
     */
	@Override
	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException {
		earlyTime = oi.readInt();
		lateTime = oi.readInt();
		employee = (Set<Employee>) oi.readObject();
		manager = (Set<Manager>) oi.readObject();
		department = (Set<Department>) oi.readObject();
		
    	comboBoxModelDepartment = new DefaultComboBoxModel();
    	for (Department dep : department) {
    		comboBoxModelDepartment.addElement(dep);
    	}
    	comboBoxModelAllEmployee = new DefaultComboBoxModel();
    	for (Employee emp : getAllEmployee()) {
    		comboBoxModelAllEmployee.addElement(emp);
    	}
	}
}