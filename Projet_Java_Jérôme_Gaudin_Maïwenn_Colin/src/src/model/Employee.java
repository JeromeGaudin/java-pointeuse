package src.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Employee class contains complex information about one employee in the company.
 */
public class Employee extends Person implements Externalizable, Comparable<Employee> {
	
    /*
     * Attributes
     */
	
    /**
	 * serialVersionUID allows to check the success of serialization.
	 */
	private static final long serialVersionUID = 4231004213423287326L;
	/**
     *overTime represents a number of over time. overTime could be negative. overTime is accurate to the nearest quarter of an hour.
     */
    private float overTime;
    /**
     * department is employee's department. Each employee has only one department.
     */
    private Department department;
    /**
     * schedule is employee's schedule. Each employee has only one schedule per day. Each day of the week is the same, but we can imagine different schedules for different days.
     */
    private DaySchedule schedule; 
    /**
     * historic contains Pointing group by two or less to have a WorkingDay.
     */
    private List<WorkingDay> historic;
    /**
     * isCurrentDay indicates if the employee finished his working day.
     */
    private boolean isCurrentDay; 
    
    /*
     * Constructors
     */
    
    /**
     * Default constructor
     */
    public Employee() {
        super();
        department = null;
        schedule = null;
        overTime = 0;
        setCurrentDay(false);
        historic = new ArrayList<WorkingDay>();
    }
    
    /**
     * Constructor
     * @param firstName0
     * @param lastName0
     */
    public Employee(String firstName0, String lastName0) {
        super(firstName0, lastName0);
        department = null;
        schedule = null;
        overTime = 0;
        setCurrentDay(false);
        historic = new ArrayList<WorkingDay>();
    }
    
    /**
     * Constructor which initialize an Employee object with more attributes.
     * @param firstName0
     * @param lastName0
     * @param dep
     * @param daySchedule
     */
    public Employee(String firstName0, String lastName0, Department dep, DaySchedule daySchedule) {
        super(firstName0, lastName0);
        department = dep;
        schedule = daySchedule;
        overTime = 0;
        setCurrentDay(false);
        historic = new ArrayList<WorkingDay>();
    }
    
    /*
     * Methods
     */
    
    //setters
    
    /**
     * This method allows to edit department attribute.
     * @param department0
     */
    public void setDepartment(Department department0){
        department = department0;
    }
   /**
    * This method allows to edit schedule attribute.
    * @param schedule0
    */
    public void setSchedule(DaySchedule schedule0){
        schedule = schedule0;
    }
    /**
     * This method allows to edit isCurrentDay attribute.
     * @param newValueCurrentDay
     */
	public void setCurrentDay(boolean newValueCurrentDay) {
		isCurrentDay = newValueCurrentDay;
	}
    
    //getters
    
    /**
     * This method returns overTime attribute.
     * @return overTime
     */
    public float getOverTime() {
        return overTime;
    }
    /**
     * This method returns department attribute.
     * @return department
     */
    public Department getDepartment(){
        return department;
    }
    /**
     * This method returns schedule attribute.
     * @return schedule
     */
    public DaySchedule getSchedule(){
        return schedule;
    }
    /**
     * This method returns isCurrentDay attribute.
     * @return isCurrentDay
     */
	public boolean getCurrentDay() {
		return isCurrentDay;
	}
	/**
	 * This methods returns historic attribute.
	 * @return historic
	 */
	public List<WorkingDay> getHistoric() {
		return historic;
	}	
    /**
     * This methods returns a list of all Pointing in the historic attribute.
     * @return List<Pointing>
     */
    public List<Pointing> getAllPointing() {
        List<Pointing> pointings = new ArrayList<Pointing>();
        
        for (WorkingDay wd : historic) {
        	pointings.add(wd.getArrival());
        	if (wd.getDeparture() != null) {
        		pointings.add(wd.getDeparture());
        	}
        }
        return pointings;
    }
	
	//other methods
	
	/**
	 * This method updates overtime attribute with a new Pointing object. This method will be called each time this employee will generate a pointing.
	 * @param pointing
	 */
    public void updateOverTime(Pointing pointing) {
    	if (isCurrentDay) {
    		long delta = ChronoUnit.MINUTES.between(schedule.getStandardArrivalTime(), pointing.getTime());
    		overTime +=  (float) -delta /60.0;
    	} else {
    		long delta = ChronoUnit.MINUTES.between(schedule.getStandardDepartureTime(), pointing.getTime());
    		overTime +=  (float) -delta /60.0;
    	}
    }
    
    /**
     * This method overshadows the method to serialize.
     * @param out
     * @throws IOException
     */
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		super.writeExternal(out);
		out.writeBoolean(isCurrentDay);
		out.writeFloat(overTime);
		out.writeObject(historic);
		out.writeObject(schedule);
		out.writeObject(department);		
	}
	
	/**
	 * This method overshadows the method to deserialize.
	 * @param in
     * @throws IOException, ClassNotFoundException
	 */
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		super.readExternal(in);
		isCurrentDay = in.readBoolean();
		overTime = in.readFloat();
		historic = (List<WorkingDay>) (ArrayList<WorkingDay>) in.readObject();
		schedule = (DaySchedule) in.readObject();
		department = (Department) in.readObject();
	}
	
	/**
	 * This method determine if a pointing is in historic or not
	 * @param pointing
	 * @return true if historic contain pointing in parameter
	 */
	public boolean containsPointing(Pointing pointing) {
		return getAllPointing().contains(pointing);
	}
	
	/**
	 * This method allows to compare two Employee objects. Thanks to this method we can sort a List<Employee>.
	 * @param emp
	 * @return int
	 */
	@Override
	public int compareTo(Employee emp) {
		return getId() - emp.getId();
	}
	
	/**
	 * This method allows to compare two Employee objects.
	 * @param o
	 * @return boolean  
	 */
	@Override
	public boolean equals(Object o) {
		boolean res = false;
		if (o != null && o.getClass() == Employee.class) {
			Employee emp = (Employee) o;
			if (emp.getId() == getId()) {
				res = true;
			}
		}
		return res;
	}
}