package src.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;

/**
 * TimeClockData class contains a List<Pointing> 
 */
public class TimeClockData implements Externalizable {
	
	 //Attributes 
	
	/**
	 * pointing is the list of Pointing kept in memory by the time clock.
	 */
    private List<Pointing> pointing;
    /**
     * defaultComboBoxModelPeople contain all information about employees in the company kept in memory by the time clock.
     */
    private DefaultComboBoxModel defaultComboBoxModelPeople;
    
    /**
     * Default constructor    
     */
    public TimeClockData() {
    	defaultComboBoxModelPeople = new DefaultComboBoxModel();
    	pointing = new ArrayList<Pointing>();
    }
    
    //getters
    
    /**
     * This methods returns defaultComboBoxModelPeople attribute.
     * @return defaultComboBoxModelPeople
     */
    public DefaultComboBoxModel getDefaultComboBoxModelPeople() {
    	return defaultComboBoxModelPeople;
    }
    /**
     * This methods returns pointing attribute.
     * @return pointing
     */
    public List<Pointing> getPointing() {
    	return pointing;
    }
    
    /**
     * This method allows to find a person in defaultComboBoxModelPeople 
     * @param idPerson to find
     * @return Person
     */
    public Person getPerson(int idPerson) {
    	for (int i = 0 ; i < defaultComboBoxModelPeople.getSize(); i++) {
    		Person person = (Person) defaultComboBoxModelPeople.getElementAt(i);
    		if (person.getId() == idPerson) {
    			return person;
    		}
    	}
    	return null;
    }
    
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(pointing);
		List<Person> list = new ArrayList<Person>();
		for (int i = 0 ; i < defaultComboBoxModelPeople.getSize(); i++) {
			list.add((Person) defaultComboBoxModelPeople.getElementAt(i));
		}
		out.writeObject(list);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		pointing = (List<Pointing>) in.readObject();
		List<Person> list = (List<Person>) in.readObject();
		defaultComboBoxModelPeople = new DefaultComboBoxModel();
		defaultComboBoxModelPeople.addAll(list);
    	
	}
}