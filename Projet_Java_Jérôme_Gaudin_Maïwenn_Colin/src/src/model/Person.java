package src.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Person class contains basic information about one employee in the company.
 */
public class Person  implements Externalizable {
	
	/*
     * Attributes
     */
	
    /**
     * id is computed thanks to idCount. id is unique, it is a login.
     */
    private int id;
    /**
     * lastName is employee's last name.
     */
    private String lastName;
    /**
     * firstName is employee's first name.
     */
    private String firstName;
    /**
     * idCount allows to  compute an unique login (id) for each employee.
     */
    private static int idCount = 0;
    
    /*
     * Constructors
     */
    
    /**
     * Default constructor
     */
    public Person() {
        id = idCount;
        idCount ++;
        firstName = "";
        lastName = "";
    }
    /**
     * Constructor
     * @param firstName0
     * @param lastName0
     */
    public Person(String firstName0, String lastName0) {
        this();
        if (firstName0==null)
            firstName = "";
        else firstName = firstName0;
        
        if (firstName0==null)
            lastName = "";
        else lastName = lastName0;
    }
    
    /*
     * Methods
     */
    
    //setters
    
    /**
     * This method allows to edit firstName attribute.
     * @param firstName0
     */
    public void setFirstName(String firstName0) {
        firstName = firstName0;
    }
    /**
     * This method allows to edit lastName attribute.
     * @param LastName0
     */
    public void setLastName(String lastName0) {
        lastName = lastName0;
    }
    
    //getters
    
    /**
     * This method returns firstName attribute.
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * This method returns lastName attribute.
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * This method returns id attribute.
     * @return id
     */
    public int getId() {
        return id;
    }
    /**
     * This method returns idCount attribute.
     * @return idCount
     */
    public static int getIdCount() {
        return idCount;
    }
    
    //other methods
    
    /**
     * This method converts a Person object to a String.
     * @return String which represents employee's first name and last name.
     */
    @Override
    public String toString() {
    	return firstName + " " + lastName;
    }
    /**
     * This method overshadows the method to serialize.
     * @param out
     * @throws IOException
     */
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(id);
		out.writeObject(lastName);
		out.writeObject(firstName);
		
	}
	 /**
     * This method overshadows the method to deserialize.
     * @param in
     * @throws IOException, ClassNotFoundException
     */
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		id = in.readInt();
		lastName = (String) in.readObject();
		firstName = (String) in.readObject();
		// update to have unique id 
		if (id > idCount) {
			idCount = id;
		}
	}
}