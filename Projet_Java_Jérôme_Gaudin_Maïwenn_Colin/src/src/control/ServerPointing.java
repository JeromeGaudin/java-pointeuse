package src.control;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.List;

import src.launcher.CentralApp;
import src.model.Pointing;

/**
 * This class contain all method for receive List<Ponting> from TimeClock thanks to TCP protocol.
 */
public class ServerPointing extends Server implements Runnable {
	/*
	 * Attribut
	 */
	
	/**
	 * centralApp allows to get all information.
	 */
	private transient CentralApp centralApp;
	
	// setter
	/**
	 * This method allows to edit centralApp
	 * @param newCentralApp
	 */
	public void setCentralApp(CentralApp newCentralApp) {
		centralApp = newCentralApp;
	}
	
	/*
	 *  Other methods
	 */
	
	/**
	 * Constructor
	 */
	public ServerPointing(CentralApp newCentralApp, String ip, int port) {
		super(port, ip);
		centralApp = newCentralApp;
	}
	
	/**
	 * This method allows to wait a connection of a socket. After connection, this method he read an List<Person> send by the socket.
	 */
	@Override
	public void run() {
		try {
			setSocket();
			Socket s;
			InputStream in;
			ObjectInputStream ois;
			
			do {
				s = getServerSocket().accept();
				//s.setSoTimeout(TIME_OUT);
				
				in = s.getInputStream();
				ois = new ObjectInputStream(in);
				try {
					List<Pointing> pointings = (List<Pointing>) readMessage(ois);

					for (Pointing poin : pointings) {
						centralApp.getCompany().addPointing(poin);
					}

				} catch (IOException e) {
					System.err.println("Erreur, il y a eu une erreur pour la lecture des donnees dans le socket");
				} catch (ClassNotFoundException e) {
					System.err.println("Erreur, mauvais type de classe");
				} finally {
					try {
						s.close();
					} catch(IOException e) {
						// do nothing
					}
				}	
				
			} while (true);
			
		}catch(IOException e){ 
			System.out.println("ServerPointing erreur de connection"); 
		}
		
	}
	
}

