package src.control;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import src.launcher.CentralApp;
import src.model.Person;

/**
 * This class allows to send List<Person>
 */
public class ClientPerson extends Client implements Runnable {
	/*
	 * Attributes
	 */
	
	/**
	 * centralApp contains all information to send. 
	 */
	private transient CentralApp centralApp;
	
	/**
	 * action to do when client run, this action can take all static final int attributes.
	 */
	private transient int action;
	/**
	 * person attribute used when client run, it indicate on which person do action.
	 */
	private transient Person person;
	/**
	 * ADD_PERSON allows to indicate to the server add person on the TimeClock.
	 */
	public static final int ADD_PERSON = 0;
	/**
	 * UPDATE_PERSON 
	 */
	public static final int UPDATE_PERSON = 1;
	public static final int DELETE_PERSON = 2;
	public static final int REPLACE_LIST_PERSON = 3;
	
	// setter
	/**
	 * This method allow to edit centralApp
	 * @param newCentralApp
	 */
	public void setCentralApp(CentralApp newCentralApp) {
		centralApp = newCentralApp;
	}

	public void setAction(int newAction) {
		action = newAction;
	}
	
	public void setPerson(Person newPerson) {
		person = newPerson;
	}

	
	/*
	 *  Other methods
	 */
	
	/**
	 * Constructor
	 * @param ca centralApp
	 * @param ip of TimeClock
	 * @param port of timeClock
	 */
	public ClientPerson(CentralApp ca, String ip, int port) {
		super(port, ip);
		centralApp = ca;
		person = null;
		action = -1;
	}
	
	/**
	 * This method launch protocol to connect to the TimeClock and send List<Person>
	 */
	@Override
	public void run() {
		try {
			setSocket();
			
			OutputStream out = getSocket().getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(out);
	
			
			// check
			switch (action) {
			case ADD_PERSON:
			case UPDATE_PERSON:
			case DELETE_PERSON:
				if (person == null) {
					throw new IllegalArgumentException("before launch run method we need to initialize person attribut");
				}
			}
			
			writeMessage(oos, action);
			switch (action) {
			case ADD_PERSON:
			case UPDATE_PERSON:
				writeMessage(oos, person);
				break;
			case DELETE_PERSON:
				writeMessage(oos, person.getId());
				break;
			case REPLACE_LIST_PERSON:
				writeMessage(oos, centralApp.getCompany().getAllEmployee());
				break;
			default:
				throw new IllegalArgumentException("before launch run method we need to initialize action attribut");
			}

			person = null;
			action = -1;
			
			getSocket().close();
		}
		catch(IOException e) { 
			System.err.println("IOException ClientPerson " + e.getMessage());
		}
	}
}
