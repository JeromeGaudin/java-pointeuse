package src.control;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.TimerTask;

import javax.swing.JOptionPane;

/**
 * This class allows to create a server TCP.
 */
public class Server implements Serializable {
	/**
	 * ss a ServerSocket permit to create connection with another socket.
	 */
	private transient ServerSocket ss;
	
	/**
	 * isA allows to store information to find receiver (ip address and port).
	 */
	private InetSocketAddress isA;
	
	// getter
	/**
	 * This method returns ss.
	 * @return ServerSocket
	 */
	public ServerSocket getServerSocket() {
		return ss;
	}
	
	/**
	 * This method returns isA.
	 * @return InetSocketAddress
	 */
	public InetSocketAddress getIsA() {
		return isA;
	}
	
	
	/*
	 * Other methods
	 */
	
	/**
	 * Default constructor
	 */
	public Server() {
		ss = null;
		isA = null;
	}
	
	/**
	 * Constructor
	 * @param port
	 */
	public Server(int port) {
		ss = null;
		isA = new InetSocketAddress("localhost", port);
	}
	
	/**
	 * Constructor
	 * @param port
	 * @param ip
	 */
	public Server(int port, String ip) {
		ss = null;
		isA = new InetSocketAddress(ip, port);
	}
	
	/**
	 * This method allows to initialize connection with receiver.
	 * @throws IOException
	 */
	protected void setSocket() throws IOException {
		if (isA == null) {
			isA = new InetSocketAddress("localhost",8080);
		}
		ss = new ServerSocket(isA.getPort());
	}
	
	/**
	 * This method allows to read a message on a ObjectInputStream.
	 * @param ois
	 * @return Object
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected Object readMessage(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		Object object = null;
		object = ois.readObject();
		return object;
	}
	
	
	/**
	 * This method allow to edit isA.
	 * @param isA
	 */
	public void setIsA(InetSocketAddress newIsA) {
		isA = newIsA;
		/*try {
			ss.close();
		} catch (IOException e) {
			// do nothing
		}
		try {
			ss = new ServerSocket(isA.getPort());
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erreur le serveur n'a pas pu demarrer sur ce port." +
					System.lineSeparator() + "Ce port est surement déjà pris.",
					"Demarrage du serveur echoue", JOptionPane.ERROR_MESSAGE);
			;
		}*/
	}
}