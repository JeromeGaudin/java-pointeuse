package src.control;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Set;

import src.model.Person;
import src.model.TimeClockData;

/**
 * This class contain all methods for get List<Person> from CentralApp.
 */
public class ServerPerson extends Server implements Runnable {
	/*
	 * Attribut
	 */
	
	/**
	 * timeClockData contains all information to send.
	 */
	private transient TimeClockData timeClockData;
	
	private static final int ADD_PERSON = 0;
	private static final int UPDATE_PERSON = 1;
	private static final int DELETE_PERSON = 2;
	private static final int REPLACE_LIST_PERSON = 3;
	
	//setter
	
	/**
	 * This method allows to edit timeClockData.
	 * @param newTimeClockData
	 */
	public void setTimeCLockData(TimeClockData newTimeClockData) {
		timeClockData = newTimeClockData;
	}
	
	/*
	 * Other methods
	 */
	
	/**
	 * Constructor
	 * @param tcd TimeClockData
	 * @param ip of server
	 * @param port of server
	 */
	public ServerPerson(TimeClockData tcd, String ip, int port) {
		super(port, ip);
		timeClockData = tcd;
	}
	
	/**
	 * This method allows to wait a connection of a socket. After connection, this method read an List<Person> send by the socket.
	 */
	@Override
	public void run() {
		try {
			setSocket();
			Socket s;
			InputStream in;
			ObjectInputStream ois;
			
			do {
				s = getServerSocket().accept();
				
				in = s.getInputStream();
				ois = new ObjectInputStream(in);
				
				try {
					int action = (int) readMessage(ois);
					
					switch (action) {
					case ADD_PERSON:
						timeClockData.getDefaultComboBoxModelPeople().addElement(readMessage(ois));
						break;
					case UPDATE_PERSON:
						Person personSocket = (Person) readMessage(ois);
						Person personData = timeClockData.getPerson(personSocket.getId());
						personData.setFirstName(personSocket.getFirstName());
						personData.setLastName(personSocket.getLastName());
						break;
					case DELETE_PERSON:
						int idPerson = (int) readMessage(ois);
						Person person = timeClockData.getPerson(idPerson);
						timeClockData.getDefaultComboBoxModelPeople().removeElement(person);
						break;
					case REPLACE_LIST_PERSON:
						Set<Person> people = (Set<Person>) readMessage(ois);
						timeClockData.getDefaultComboBoxModelPeople().removeAllElements();
						timeClockData.getDefaultComboBoxModelPeople().addAll(people);
						break;
					default:
						throw new IllegalArgumentException("Data on socket are incorrect");
					}
					
				} catch (IOException e) {
					System.err.println("Erreur, il y a eu une erreur pour la lecture des donnees dans le socket");
				} catch (ClassNotFoundException e) {
					System.err.println("Erreur, mauvais type de classe");
				} finally {
					try {
						s.close();
					} catch(IOException e) {
						// do nothing
					}
				}

			} while (true);
			
		}catch(IOException e){ 
			System.err.println("ServerPerson erreur de connection"); 
		}
		
	}
	
}

