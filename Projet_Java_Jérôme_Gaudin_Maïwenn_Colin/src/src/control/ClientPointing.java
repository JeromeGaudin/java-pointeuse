package src.control;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.swing.JOptionPane;

import src.model.TimeClockData;
import src.view.MainFrameTimeClock;

/**
 * This class contain all methods to send a List<Pointing> to CentralApp
 */
public class ClientPointing extends Client implements Runnable {
	/*
	 * Attributs
	 */
	
	/**
	 * timeClockData allows to have access to all Pointings.
	 */
	private transient TimeClockData timeClockData;
	
	/**
	 * mainFrame is the view when the action to send List<Pointing> are request.
	 */
	private transient MainFrameTimeClock mainFrame; 
	
	// setter
	/**
	 * This method allows to edit timeClockData.
	 * @param timeClockData
	 */
	public void setTimeClockData(TimeClockData newTimeClockData) {
		timeClockData = newTimeClockData;
	}
	
	/**
	 * This method allows to edit mainFrame.
	 * @param mainFrame
	 */
	public void setMainFrame(MainFrameTimeClock newMainFrame) {
		mainFrame = newMainFrame;
	}
	
	/*
	 * Other methods
	 */
	
	/**
	 * Constructor
	 * @param newTimeClockData
	 * @param mainFrameTimeClock
	 * @param ip of CentralApp server
	 * @param port of port server
	 */
	public ClientPointing(TimeClockData newTimeClockData, MainFrameTimeClock mainFrameTimeClock, String ip, int port) {
		super(port, ip);
		mainFrame = mainFrameTimeClock;
		timeClockData = newTimeClockData;
	}
	
	/**
	 * The method to launch protocol to send List<Pointing> to CentralApp.
	 */
	@Override
	public void run() {
		try {
			setSocket();
			
			OutputStream out = getSocket().getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(out);
			try {
				writeMessage(oos, timeClockData.getPointing());
	
				timeClockData.getPointing().removeAll(timeClockData.getPointing());
				
			} catch(IOException e) {
				JOptionPane.showMessageDialog(mainFrame, "Erreur les données n'ont pas pus être envoyé, problème lors de l'envoie des données",
						"Synchronysation Echoue", JOptionPane.ERROR_MESSAGE)
				;
			} finally {
				try {
					getSocket().close();
				} catch(IOException e) {
					// do nothing
				}
			}
		} catch(IOException e) {
			JOptionPane.showMessageDialog(mainFrame, "Erreur les données n'ont pas pus être envoyé, l'application central n'est pas accessible",
					"Synchronysation Echoue", JOptionPane.ERROR_MESSAGE)
			;
		}
	}
}
