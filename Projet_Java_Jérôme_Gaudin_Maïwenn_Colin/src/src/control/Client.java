package src.control;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * This class Allows to create and use a client TCP.
 */
public class Client implements Serializable {
	/*
	 * Attributes
	 */
	
	/**
	 * s allows to communicate with an another socket
	 */
	private transient Socket s;
	/**
	 * isA allows to store all information for a connection (ip address and port) 
	 */
	private InetSocketAddress isA;
	
	// setter
	/**
	 * This method allows to edit isA 
	 * @param newIsA
	 */
	public void setIsA(InetSocketAddress newIsA) {
		isA = newIsA;
	}
	// getter
	/**
	 * This method returns s
	 * @return Socket
	 */
	public Socket getSocket() {
		return s;
	}
	
	/**
	 * This method returns isA
	 * @return
	 */
	public InetSocketAddress getIsA() {
		return isA;
	}
	
	/*
	 * Others Methods
	 */
	
	/**
	 * Default constructor
	 */
	public Client() {
		s = null;
		isA = null;
	}
	
	/**
	 * Constructor
	 * @param port
	 */
	public Client(int port) {
		s = null;
		isA = new InetSocketAddress("localhost", port);
	}
	
	/**
	 * Constructor
	 * @param port
	 * @param ip
	 */
	public Client(int port, String ip) {
		s = null;
		isA = new InetSocketAddress(ip, port);
	}
	
	/**
	 * setSocket initialize the connection with the server.
	 * @throws IOException if the server is unreachable.
	 */
	protected void setSocket() throws IOException {
		if (isA == null) {
			isA = new InetSocketAddress("localhost",8080);
		}
		s = new Socket(isA.getHostName(), isA.getPort());
	}
	
	/**
	 * This method allows to write and send a message to the destinator
	 * @param oos
	 * @param object to send 
	 * @throws IOException
	 */
	protected void writeMessage(ObjectOutputStream oos, Object object) throws IOException {
		oos.writeObject(object);
		oos.flush();
	}
}