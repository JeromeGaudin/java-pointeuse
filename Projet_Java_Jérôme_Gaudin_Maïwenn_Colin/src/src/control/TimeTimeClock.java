package src.control;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import src.model.Pointing;
import src.view.MainFrameTimeClock;

/**
 * This class permit to change the date of MainFrameTimeClock.
 */
public class TimeTimeClock implements Runnable {
	/*
	 * Attributs
	 */
	
	/**
	 * mftc is the main frame to change time.
	 */
	private MainFrameTimeClock mftc;
	
	/*
	 * Other methods 
	 */
	
	/**
	 * Constructor
	 * @param mainFrameTimeClock
	 */
	public TimeTimeClock(MainFrameTimeClock mainFrameTimeClock) {
		mftc = mainFrameTimeClock;
	}

	/**
	 * This method allows to change date of MainFrameTimeClock
	 */
	@Override
	public void run() {
		LocalTime time = LocalTime.now();
		time = time.minusSeconds(time.getSecond());
		time = time.minusNanos(time.getNano());
		
		int timeMinuteBefore  = -1;
		
		while (true) {
			if (time.getMinute() != timeMinuteBefore) {
				LocalDateTime dateTimeRound = Pointing.round(time.atDate(LocalDate.now()));
				
				mftc.setTime(time.toString() + " so it's " + dateTimeRound.toLocalTime());
				timeMinuteBefore = time.getMinute();
			}
			time = LocalTime.now();
		}
	}

}
