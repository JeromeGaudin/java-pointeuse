package src.launcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.swing.JOptionPane;

import src.control.ClientPerson;
import src.control.ServerPointing;
import src.model.Company;
import src.model.DaySchedule;
import src.model.Department;
import src.model.Employee;
import src.model.Person;
import src.model.Pointing;
import src.view.WindowCentralApp;
/**
 * CentralApp class contains all useful attributes to launch central application and to interact with it.
 */
public class CentralApp {
	/**
	 * Company contains all data for company.
	 */
	private Company company;
	/**
	 * This is the name of file allows to save data of centralApp.
	 */
	private static String backupFileName = "backupFileCentralApp.dat";
	/**
	 * clientPerson represents client part of central application to send List<Person> to TimeClock.
	 */
	private ClientPerson clientPerson;
	/**
	 * serverPointing represents server part of central application to receive List<Pointing> from TimeClock.
	 */
	private ServerPointing serverPointing;
	/**
	 * window represent part which is printed on screen. It will contain all tabs.
	 */
	private WindowCentralApp window;

	/**
     * Default constructor
     */
    public CentralApp() {
    	restoreData();
    	if (company == null) {
    		company = new Company();
    	}
    	if (serverPointing == null) {
        	serverPointing = new ServerPointing(this, "localhost", 55556);	
    	}
    	if (clientPerson == null) {
        	clientPerson = new ClientPerson(this, "localhost", 55555);	
    	}
    	window = new WindowCentralApp(this);
    	window.setVisible(true);
    	
    	new Thread(serverPointing).start();

    	sendPeople(ClientPerson.REPLACE_LIST_PERSON, null);
    }
    
    // setters
    /**
     * This method allows to edit backupFileName, useful for tests
     * @param name
     */
    public static void setBackupFileName(String name) {
    	backupFileName = name;
    }
    
    // Getter
    
    /**
     * This method returns window attribute.
     * @return WindowCentralApp
     */
    public WindowCentralApp getWindow() {
    	return window;
    }
    /**
     * This method returns company attribute.
     * @return Company
     */
    public Company getCompany() {
    	return company;
    }
    
    /**
     * This method returns clientPerson attribute.
     * @return clientPerson
     */
	public ClientPerson getClientPerson() {
		return clientPerson;
	}
	
	/**
	 * This method returns serverPointing attribute.
	 * @return serverPointing
	 */
	public ServerPointing getServerPointing() {
		return serverPointing; 
	}

    /*
     * Other methods
     */
    
    /**
     * This method allows to save data from centralApp in file named backupFileName thanks to serialization. 
     */
    public void saveData() {
		ObjectOutputStream oos = null;
		
		try {
			FileOutputStream fos = new FileOutputStream(backupFileName);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(company);
			oos.writeObject(serverPointing);
			oos.writeObject(clientPerson);
			
		} catch (FileNotFoundException e) {
			String msg = "Erreur le programme ne peut pas écrire dans le fichier\"" + backupFileName + "\""
					+ System.lineSeparator() + "Les données ne seront pas enregistre"
			; 
			JOptionPane.showMessageDialog(null, msg,
					"Sauvegarde des donnees echoue", JOptionPane.ERROR_MESSAGE)
			;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Il y a eu une erreur dans l'écriture des données",
					"sauvegarde des donnees echoue", JOptionPane.ERROR_MESSAGE)
			;
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				// do nothing
			}
		}	
    }
    
    /**
     * This method allows to restore data from file named backupFileName to centralApp thanks to deserialization. 
     */
    public void restoreData() {
		ObjectInputStream ois = null;
		File file = new File(backupFileName); 
		
		if ( file.exists()) {
			try {
				FileInputStream fis = new FileInputStream(file);
				ois =new ObjectInputStream(fis);
				company = (Company) ois.readObject();
				
				serverPointing = (ServerPointing) ois.readObject();
				serverPointing.setCentralApp(this);
				
				clientPerson = (ClientPerson) ois.readObject();
				clientPerson.setCentralApp(this);
				
				ois.close();
			} catch (FileNotFoundException e) {
				String msg = "Erreur le programme ne peut pas lire dans le fichier\"" + backupFileName + "\""
						+ System.lineSeparator() + "Il ne pourra donc pas enregistrer les données"
				;
				JOptionPane.showMessageDialog(null, msg,
						"Recuperation des donnees echoue", JOptionPane.ERROR_MESSAGE)
				;
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Erreur les donnees enregistre ne peuvent pas etre recupere",
						"Recuperation des donnees echoue", JOptionPane.ERROR_MESSAGE)
				;
				company = null;
				serverPointing = null;
				clientPerson = null;
			} catch (ClassNotFoundException e) {
				System.err.println("Erreur du programmeur");
				e.printStackTrace();
			}
		}
    }
    
    /**
     * This method allows to call method to send List<Person> to TimeClock thanks to a new thread.
     * @param action to do, see static attributes of ClientPerson
     * @param person the person to send if the action are to send a person
     */
    public void sendPeople(int action, Person person) {
    	clientPerson.setAction(action);
    	clientPerson.setPerson(person);
    	new Thread(clientPerson).start();
    }
    
    /**
     * This is the main method.
     * @param args
     */
    public static void main(String[] args) {
    	CentralApp centralApp = new CentralApp();
    }
}