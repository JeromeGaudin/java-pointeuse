package src.launcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JOptionPane;

import src.control.ClientPointing;
import src.control.ServerPerson;
import src.model.TimeClockData;
import src.view.WindowTimeClock;

/**
 * TimeCLock class contains all useful attributes to launch TimeCock and to interact with it.
 */
public class TimeClock {
    /*
     * Attributes
     */
	
    /**
     * This is the name of file allows to save data of TimeClock.
     */
	private static String backupFileName = "backupFileTimeClock.dat";
	/**
	 * timeClockData contains all data for TimeClock.
	 */
	private TimeClockData timeClockData;
	/**
	 * window represent part which is printed on screen. It will contain all tabs.
	 */
    private WindowTimeClock window;
    /**
     * serverPerson represents server part of TimeClock to receive List<Person> from CentralApp.
     */
    private ServerPerson serverPerson;
    /**
     * clientPointing represents client part of TimeClock to send List<Pointing> to CentralApp.
	 */
    private ClientPointing clientPointing;

    /**
     * Default constructor
     */
    public TimeClock() {

    	restoreData();
    	if (timeClockData == null) {
    		timeClockData = new TimeClockData();
    	}
    	if (serverPerson == null) {
        	serverPerson = new ServerPerson(timeClockData, "localhost", 55555);
    	}
    	window = new WindowTimeClock(this);
    	if (clientPointing == null) {
        	clientPointing = new ClientPointing(timeClockData, window.getMainFrame(), "localhost", 55556);	
    	} else {
			clientPointing.setMainFrame(window.getMainFrame());
    	}
    	new Thread(serverPerson).start();
    	window.setVisible(true);
    }
    
    // setters
    /**
     * This method allows to edit backupFileName, useful for tests
     * @param name
     */
    public static void setBackupFileName(String name) {
    	backupFileName = name;
    }
    
    // getter
    
    /**
     * This method returns timeClockData attribute.
     * @return timeClockData
     */
    public TimeClockData getTimeClockData() {
    	return timeClockData;
    }
    
    /**
     * This method returns serverPerson attribute.
     * @return serverPerson
     */
	public ServerPerson getServerPerson() {
		return serverPerson;
	}
	
	/**
     * This method returns clientPointing attribute.
     * @return clientPointing
     */
	public ClientPointing getClientPointing() {
		return clientPointing;
	}
    
    /*
     * Other methods
     */


    /**
     * This method allows to save data from TimeClock in file named backupFileName thanks to serialization. 
     */
    public void saveData() {
    	ObjectOutputStream oos = null;
		
		try {
			FileOutputStream fos = new FileOutputStream(backupFileName);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(timeClockData);
			oos.writeObject(serverPerson);
			oos.writeObject(clientPointing);
			
		} catch (FileNotFoundException e) {
			String msg = "Erreur le programme ne peut pas écrire dans le fichier\"" + backupFileName + "\""
					+ System.lineSeparator() + "Les données ne seront pas enregistre"
			; 
			JOptionPane.showMessageDialog(null, msg,
					"Sauvegarde des donnees echoue", JOptionPane.ERROR_MESSAGE)
			;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Il y a eu une erreur dans l'écriture des données",
					"sauvegarde des donnees echoue", JOptionPane.ERROR_MESSAGE)
			;
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				// do nothing
			}
		}	
    }

    /**
     * This method allows to restore data from file named backupFileName to TimeClock thanks to deserialization. 
     */
    public void restoreData() {
    	ObjectInputStream ois = null;
		File file = new File(backupFileName); 
		
		if ( file.exists()) {
			try {
				FileInputStream fis = new FileInputStream(file);
				ois =new ObjectInputStream(fis);
				timeClockData = (TimeClockData) ois.readObject();
				
				serverPerson = (ServerPerson) ois.readObject();
				serverPerson.setTimeCLockData(timeClockData);
				
				clientPointing = (ClientPointing) ois.readObject();
				clientPointing.setTimeClockData(timeClockData);
				
				ois.close();
			} catch (FileNotFoundException e) {
				String msg = "Erreur le programme ne peut pas lire dans le fichier\"" + backupFileName + "\""
						+ System.lineSeparator() + "Il ne pourra donc pas enregistrer les données"
				; 
				JOptionPane.showMessageDialog(null, msg,
						"Recuperation des donnees echoue", JOptionPane.ERROR_MESSAGE)
				;
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Erreur les donnees enregistre ne peuvent pas etre recupere",
						"Recuperation des donnees echoue", JOptionPane.ERROR_MESSAGE)
				;
				timeClockData = null;
				serverPerson = null;
				clientPointing = null;
			} catch (ClassNotFoundException e) {
				System.err.println("Erreur du programmeur");
				e.printStackTrace();
			}
		}
    }

    /**
     * This method allows to call method to send List<Pointing> to CentralApp thanks to a new thread.
     */
    public void synchronization() {
    	if ( !timeClockData.getPointing().isEmpty()) {
        	new Thread(clientPointing).start();	
    	}
    }
	
    /**
     * This is the main method.
     * @param args
     */
    public static void main(String[] args) {
    	TimeClock timeClock = new TimeClock();    	
    }
}