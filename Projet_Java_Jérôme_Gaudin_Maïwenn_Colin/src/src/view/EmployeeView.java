package src.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import src.control.ClientPerson;
import src.launcher.CentralApp;
import src.model.Department;
import src.model.Employee;
import src.model.Manager;
import src.model.Person;
import src.view.component.LocalTimeInput;

/**
 * EmployeeView class contains all attributes to print tab to show and edit an Employee and its attributes.
 */
public class EmployeeView  extends JPanel implements ActionListener, View {
	/**
	 * centralApp allows to have access of all information needed.
	 */
	private CentralApp centralApp;
	/**
	 * valider is button to confirm edition of selected Employee.
	 */
    private JButton valider;
	/**
	 * supprimer is button to confirm suppression of selected Employee.
	 */
    private JButton supprimer;
    /**
     * comboBoxEmployees contains all employees.
     */
    private JComboBox comboBoxEmployees;
    /**
     * comboBoxDepartment contains all departments.
     */
	private JComboBox comboBoxDepartment;
    /**
     * choice allows to answer.
     */
    public static final String[] choice = {"non" ,"oui"};
    /**
     * txtLastName allows to input employee's new last name.
     */
	private JTextField txtLastName;
    /**
     * txFirstName allows to input employee's new first name.
     */
	private JTextField txtFirstName;
	/**
	 * hA corresponds to standardArrivalTime in employee's schedule.
	 */
	private LocalTimeInput hA;
	/**
	 * hD corresponds to standardDepartureTime in employee's schedule.
	 */
	private LocalTimeInput hD;
    /**
     * labelEmpId allows to read selected employee's id.
     */
    private JLabel labelEmpId;
    /**
     * labelEmpDep allows to read selected employee's Department.
     */
    private JLabel labelEmpDep;
    /**
     * labelEmpHA allows to read selected employee's standardarrivalTime in schedule attribute.
     */
    private JLabel labelEmpHA;
    /**
     * labelEmpHD allows to read selected employee's standardDepartureTime in schedule attribute.
     */
    private JLabel labelEmpHD;
    /**
     * labelEmpHSup allows to read selected employee's overTime.
     */
    private JLabel labelEmpHSup;
    /**
     * labelEmpManager allows to know if selected employee is a manager.
     */
    private JLabel labelEmpManager;
    /**
     * labelEmpManager allows to know if selected employee manager who manage his department.
     */
    private JLabel labelEmpManagerActif;
    /**
     * labelEmpManager indicates where we can know if selected employee manager who manage his department.
     */
    private JLabel labelManagerActif;
    
    /**
     * Constructor
     * @param centralApp0
     */
    public EmployeeView(CentralApp centralApp0) {
    //start
    	centralApp = centralApp0;
    	setBorder(new EmptyBorder(10, 10, 10, 10));
	   	setLayout(new GridLayout(1, 2, 60, 0));
	   	JPanel leftPanel = new JPanel();
	   	leftPanel.setLayout(new GridLayout(9, 2, 20, 20));
	   	JPanel rightPanel = new JPanel ();
	   	rightPanel.setLayout(new GridLayout(6, 2, 20, 20));
    
    //left panel
	   	
	   	comboBoxEmployees = new JComboBox(centralApp.getCompany().getComboBoxModelAllEmployee());
	   	comboBoxEmployees.addActionListener(this);
	   	leftPanel.add(comboBoxEmployees);
	   	leftPanel.add(new JPanel());

	   	JLabel labelId = new JLabel("Identifiant");
	   	JLabel labelDep = new JLabel("Département");
	   	JLabel labelHA = new JLabel("Heure d'arrivée");
	   	JLabel labelHD = new JLabel("Heure de départ");
	   	JLabel labelHSup = new JLabel("Heures supplémentaires");
	   	JLabel labelManager = new JLabel("Manager");
	   	labelManagerActif = new JLabel("Manager actif");
	   	 
	   	labelEmpId = new JLabel();
	    labelEmpDep = new JLabel();
	    labelEmpHA = new JLabel();
	    labelEmpHD = new JLabel();
	    labelEmpHSup = new JLabel();
	    labelEmpManager = new JLabel();
	    labelEmpManagerActif = new JLabel();
	   	 
	   	leftPanel.add(labelId);
	   	leftPanel.add(labelEmpId);
	   	leftPanel.add(labelDep);
	   	leftPanel.add(labelEmpDep);
	   	leftPanel.add(labelHA);
	   	leftPanel.add(labelEmpHA);
	   	leftPanel.add(labelHD);
	   	leftPanel.add(labelEmpHD);
	   	leftPanel.add(labelHSup);
	   	leftPanel.add(labelEmpHSup);
	   	leftPanel.add(labelManager);
	   	leftPanel.add(labelEmpManager);
	   	leftPanel.add(labelManagerActif);
	   	leftPanel.add(labelEmpManagerActif);
   	     
	   	leftPanel.add(new JPanel()); // panel vide
	   	supprimer =  new JButton("supprimer");
	   	supprimer.addActionListener(this);
	   	leftPanel.add(supprimer);
	   	
	   	// right panel
	   	
	   	rightPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Modifier"),
                BorderFactory.createEmptyBorder(10,10,10,10))
		);

    	JLabel labelLastName = new JLabel("Nom");
    	JLabel labelFirstName = new JLabel("Prenom");
    	JLabel labelDepartment = new JLabel("Département");
    	txtLastName = new JTextField();
    	txtFirstName = new JTextField();
   	 
    	rightPanel.add(labelLastName);
    	rightPanel.add(txtLastName);
    	rightPanel.add(labelFirstName);
    	rightPanel.add(txtFirstName);
    	
    	labelHA = new JLabel("Heure d'arrivée");
    	labelHD = new JLabel("Heure de départ");
    	hA = new LocalTimeInput();
    	hD = new LocalTimeInput();
   	 
    	rightPanel.add(labelHA);
    	rightPanel.add(hA);
    	rightPanel.add(labelHD);
    	rightPanel.add(hD);
    	
    	rightPanel.add(labelDepartment);
    	comboBoxDepartment = new JComboBox(centralApp.getCompany().getComboBoxModelDepartment());
    	rightPanel.add(comboBoxDepartment);
	   	
	   	rightPanel.add(new JPanel());
    	valider =  new JButton("valider");
    	valider.addActionListener(this);
    	rightPanel.add(valider);
	//end
	   	add(leftPanel);
	   	add(rightPanel);
    }
    
    /**
     * This method overshadows actionPerformed method. Thanks to this method we can edit attributes of an Employee.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
	   	if (e.getSource()==valider) {
	   		// check
    		if (comboBoxEmployees.getSelectedItem() == null) {
    			JOptionPane.showMessageDialog(this, "Il faut dabord cree un employee");
    			return;
    		}
    		
    		// edition
    		Employee emp = (Employee) comboBoxEmployees.getSelectedItem();
    		
    		String newFristName = txtFirstName.getText().trim();
    		if ( !newFristName.equals("")) {
    			emp.setFirstName(newFristName);
    		}
    		String newLastName = txtLastName.getText().trim();
    		if ( !newLastName.equals("")) {
    			emp.setLastName(newLastName);
    		}
    		Department dep = (Department) comboBoxDepartment.getSelectedItem();
    		if (emp.getDepartment() == null || dep.getId() != emp.getDepartment().getId()) {
    			emp.setDepartment(dep);
    		}
    		if (hA.readLocalTime() != emp.getSchedule().getStandardArrivalTime()) {
    			emp.getSchedule().setStandardArrivalTime(hA.readLocalTime());
    		}
    		if (hD.readLocalTime() != emp.getSchedule().getStandardDepartureTime()) {
    			emp.getSchedule().setStandardDepartureTime(hD.readLocalTime());
    		}
    		centralApp.sendPeople(ClientPerson.UPDATE_PERSON, (Person) emp);
    		
    		updateView();
    		updateUI();
    		
	   	}
	   	else if (e.getSource()==supprimer) {
	   		// check
    		if (comboBoxEmployees.getSelectedItem() == null) {
    			JOptionPane.showMessageDialog(this, "Il faut dabord cree un employee");
    			return;
    		}
    		Employee emp = (Employee) comboBoxEmployees.getSelectedItem();
    		centralApp.getCompany().getComboBoxModelAllEmployee().removeElement(emp);
    		centralApp.getWindow().getPointing().getDefaultComboBoxModelEmployee().removeElement(emp);
    		
    		if (centralApp.getCompany().isManager(emp.getId())) {
    			centralApp.getCompany().removeManager(emp.getId());
	   		} else {
	   			centralApp.getCompany().removeEmployee(emp.getId());
	   		}
    		centralApp.sendPeople(ClientPerson.DELETE_PERSON, (Person) emp);
	   	}
	   	else if (e.getSource() == comboBoxEmployees) {
	   		if (comboBoxEmployees.getSelectedItem() != null) {

		   		Employee emp = (Employee) comboBoxEmployees.getSelectedItem();
		   		labelEmpId.setText(Integer.toString(emp.getId()));
		   		if (emp.getDepartment() == null ) {
		   			labelEmpDep.setText("Pas de departement");
		   			labelEmpDep.setForeground(Color.RED);
		   		} else {
		   			labelEmpDep.setText(emp.getDepartment().getName());
		   			labelEmpDep.setForeground(Color.BLACK);
		   		}
			    labelEmpHA.setText(emp.getSchedule().getStandardArrivalTime().toString());
			    labelEmpHD.setText(emp.getSchedule().getStandardDepartureTime().toString());
			    hA.writeLocalTime(emp.getSchedule().getStandardArrivalTime());
			    hD.writeLocalTime(emp.getSchedule().getStandardDepartureTime());
			    
			    labelEmpHSup.setText(Float.toString(emp.getOverTime()));
			    if (centralApp.getCompany().isManager(emp.getId())) {
			    	labelEmpManager.setText("oui");
			    	labelManagerActif.setVisible(true);
			    	labelEmpManagerActif.setVisible(true);
			    	Manager manager = centralApp.getCompany().getManager(emp.getId());
			    	if (manager.getManage()) {
			    		 labelEmpManagerActif.setText("oui");
			    	} else {
			    		labelEmpManagerActif.setText("non");
			    	}
			    } else {
			    	labelEmpManager.setText("non");
			    	labelManagerActif.setVisible(false);
			    	labelEmpManagerActif.setVisible(false);
			    	
			    }
	   		}
	   	}
    }

	/**
	 * This method allows to update information after manipulation on this tab.
	 */
	@Override
	public void updateView() {
		// simulates select item on comboBoxDep JComboBox
		ActionEvent actionEvent = new ActionEvent(comboBoxEmployees, ActionEvent.ACTION_PERFORMED, "");
	    this.actionPerformed(actionEvent);
	}    
}