package src.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import src.control.ClientPerson;
import src.launcher.CentralApp;
import src.model.DaySchedule;
import src.model.Department;
import src.model.Employee;
import src.model.Manager;
import src.model.Person;
import src.view.component.LocalTimeInput;

/**
 * AddEmployeeView class contains all attributes to print tab to add an employee.
 */
public class AddEmployeeView  extends JPanel implements ActionListener, View {

	/*
	 * Attributes
	 */
	
	/**
	 * centralApp allows to have access of all information needed.
	 */
	private CentralApp centralApp;
	/**
	 * valider is button to confirm employee's addition.
	 */
	private JButton valider;
    /**
     * comboBoxDepartment contains all departments.
     */
	private JComboBox comboBoxDepartment;
	/**
	 * buttonManager allows to now if the new employee is a manager.
	 */
	private JComboBox buttonManager;
	/**
	 * buttonActiveManager allows to now if the new employee manage his department.
	 */
	private JComboBox buttonActiveManager;
    /**
     * choice allows to answer.
     */
	public static final String[] choice = {"non" ,"oui"};
    /**
     * txtLastName allows to input employee's last name.
     */
	private JTextField txtLastName;
    /**
     * txFirstName allows to input employee's first name.
     */
	private JTextField txtFirstName;
	/**
	 * arrivalTime corresponds to standardArrivalTime in employee's schedule.
	 */
	private LocalTimeInput arrivalTime;
	/**
	 * departureTime corresponds to standardDepartureTime in employee's schedule.
	 */
	private LocalTimeInput departureTime;
	
	/*
	 *Methods 
	 */
	
	/**
 	* Constructor
 	* @param centralApp0
 	*/
	public AddEmployeeView(CentralApp centralApp0) {
    	centralApp = centralApp0;
    	
    	//start
    	setBorder(new EmptyBorder(10, 10, 10, 10));
    	setLayout(new GridLayout(1,2, 100, 0));
    	JPanel leftPanel = new JPanel ();
    	leftPanel.setLayout(new GridLayout(4, 2, 20, 100));
    	JPanel rightPanel = new JPanel ();
    	rightPanel.setLayout(new GridLayout(4,2, 20, 100));
   	 
    	//left
    	JLabel labelLastName = new JLabel("Nom");
    	JLabel labelFirstName = new JLabel("Prenom");
    	JLabel labelArrivalTime = new JLabel("Heure d'arrivée");
    	JLabel labelDepartureTime = new JLabel("Heure de départ");
    	arrivalTime = new LocalTimeInput();
    	departureTime = new LocalTimeInput();
    	
    	txtLastName = new JTextField();
    	txtFirstName = new JTextField();
   	 
    	leftPanel.add(labelLastName);
    	leftPanel.add(txtLastName);
    	leftPanel.add(labelFirstName);
    	leftPanel.add(txtFirstName);
    	leftPanel.add(labelArrivalTime);
    	leftPanel.add(arrivalTime);
    	leftPanel.add(labelDepartureTime);
    	leftPanel.add(departureTime);
   	 
   	 
    	//right
    	JLabel labelDepartment = new JLabel("Département");
    	JLabel labelManager = new JLabel("Manager");
    	JLabel labelManagerActif = new JLabel("Manager actif");
   	 
    	rightPanel.add(labelDepartment);
    	comboBoxDepartment = new JComboBox(centralApp.getCompany().getComboBoxModelDepartment());
    	rightPanel.add(comboBoxDepartment);
    
    	rightPanel.add(labelManager);
    	buttonManager = new JComboBox(choice);
    	buttonManager.addActionListener(this);
    	rightPanel.add(buttonManager);
    	
    	rightPanel.add(labelManagerActif);
    	buttonActiveManager = new JComboBox(choice);
    	buttonActiveManager.setEnabled(false);
    	rightPanel.add(buttonActiveManager);
    	
    	rightPanel.add(new JPanel());
    	
    	valider =  new JButton("valider");
    	valider.addActionListener(this);
    	rightPanel.add(valider);
   	 
    	//fin
    	add(leftPanel);
    	add(rightPanel);
	}
    /**
     * This method overshadows actionPerformed method. Thanks to this method we can add a new employee.
     */
	@Override
	public void actionPerformed(ActionEvent e) {
    	if (e.getSource() == valider) {
    		
    		// check
    		
    		if (comboBoxDepartment.getSelectedItem() == null) {
    			JOptionPane.showMessageDialog(this, "Il faut dabord crée un département");
    			return;
    		}
    		String lastName = txtLastName.getText().trim();
    		if (lastName.equals("")) {
    			JOptionPane.showMessageDialog(this, "Le champ du nom doit être remplit");
    			return;
    		}
    		String firstName = txtFirstName.getText().trim();
    		if (lastName.equals("")) {
    			JOptionPane.showMessageDialog(this, "Le champ du prénom doit être remplit");
    			return;
    		}

    		// construction
    		DaySchedule schedule = new DaySchedule(arrivalTime.readLocalTime(), departureTime.readLocalTime());
    		int idDep = ((Department) comboBoxDepartment.getSelectedItem()).getId();
    		Department department = centralApp.getCompany().getDepartment(idDep);
    		

    		Person PersonToSend = null;
    		if (buttonManager.getSelectedItem().equals("oui")) {
    			//manager
    			boolean managerActif = buttonActiveManager.getSelectedItem().equals("oui");
    			Manager manager = new Manager(firstName, lastName, department, schedule, managerActif);
    			PersonToSend = (Person) manager;
    			centralApp.getCompany().addManager(manager);
        		centralApp.getCompany().getComboBoxModelAllEmployee().addElement( (Employee) manager);
        		centralApp.getWindow().getPointing().getDefaultComboBoxModelEmployee().addElement((Employee) manager);
    		} else {
        		// empoyee
    			Employee employee = new Employee(firstName, lastName, department, schedule);
    			PersonToSend = (Person) employee;
    			centralApp.getCompany().addEmployee(employee);
        		centralApp.getCompany().getComboBoxModelAllEmployee().addElement(employee);
        		centralApp.getWindow().getPointing().getDefaultComboBoxModelEmployee().addElement(employee);
    		}
    		centralApp.sendPeople(ClientPerson.ADD_PERSON, PersonToSend);
    		
    		// end
    		buttonManager.setSelectedIndex(0);
    		txtLastName.setText("");
    		txtFirstName.setText("");
    		
    	} 
    	else if (e.getSource() == buttonManager) {
    		if (buttonManager.getSelectedItem().equals("oui")) {
    			buttonActiveManager.setEnabled(true);
    		} else {
    			buttonActiveManager.setSelectedIndex(0);
    			buttonActiveManager.setEnabled(false);
    		}
    	}
   	 
	}

	/**
	 * This method allows to update information after manipulation on this tab.
	 */
	@Override
	public void updateView() {
		// nothing
	}
}