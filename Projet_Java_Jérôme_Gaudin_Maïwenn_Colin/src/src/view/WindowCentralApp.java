package src.view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import src.launcher.CentralApp;

/**
 * WindowCentralApp class contains all attributes to print tab to contain all tabs for CentralApp.
 */
public class WindowCentralApp extends JFrame implements WindowListener, ChangeListener {
	/**
	 * centralApp allows to have access of all information needed.
	 */
	private CentralApp centralApp;
	/**
	 * departmentView to show and edit a Department.
	 */
	private DepartmentView departmentView;
	/**
	 * pointing allows to show Pointing.
	 */
	private PointingView pointing;
	/**
	 * tabbedPane contains all tabs.
	 */
	private JTabbedPane tabbedPane;
	
	/**
	 * Constructor
	 * @param newCentralApp
	 */
	public WindowCentralApp(CentralApp newCentralApp) {
		centralApp = newCentralApp;
		
		// window setting
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(950, 500));
		setSize(1000, 600);
		setTitle("Application central");
		addWindowListener(this);
		
		// layout
		setLayout(new GridLayout(1, 1));
		
		tabbedPane = new JTabbedPane();
		
		// Pointing
		pointing = new PointingView(centralApp);
		tabbedPane.addTab("Pointage", pointing);
		
		// EmployeeView
		EmployeeView mo = new EmployeeView(centralApp);
		tabbedPane.addTab("Employé", mo);
		
		// AddEmployeeView
		AddEmployeeView mo2 = new AddEmployeeView(centralApp);
		tabbedPane.addTab("Ajouter employe", mo2);
		
		// DepartmentView
		departmentView = new DepartmentView(centralApp);
		tabbedPane.addTab("Département", departmentView);
		
		// CentralAppSetting
		CentralAppSettings centralAppSettings = new CentralAppSettings(centralApp);
		tabbedPane.addTab("Parametres", centralAppSettings);
		
		tabbedPane.addChangeListener(this);
		
		// update first panel
		View view = (View) tabbedPane.getSelectedComponent();
		view.updateView();
		
		add(tabbedPane);
	}
	
	// Listener for the window

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	/**
	 * This method allows to save data when window is closed.
	 * @param arg0
	 */
	@Override
	public void windowClosing(WindowEvent arg0) {
		centralApp.saveData();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}
	
	/**
	 * This method returns departmentView attribute.
	 * @returndepartmentView
	 */
	public DepartmentView getDepartmentView() {
		return departmentView;
	}
	/**
	 * This method returns pointing attribute.
	 * @return pointing
	 */
	public PointingView getPointing() {
		return pointing;
	}
	/**
	 * This method allows to update chosen tab. It is called each time we change tab. 
	 * @param event
	 */
	@Override
	public void stateChanged(ChangeEvent event) {
		View view = (View) tabbedPane.getSelectedComponent();
		view.updateView();
	}
}