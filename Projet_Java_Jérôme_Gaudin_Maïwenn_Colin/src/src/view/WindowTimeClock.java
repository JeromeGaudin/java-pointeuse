package src.view;

import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import src.control.TimeTimeClock;
import src.launcher.TimeClock;

/**
 * WindowTimeClock class contains all attributes to print tab to contain all tabs for TimeClock.
 */
public class WindowTimeClock extends JFrame implements WindowListener, ChangeListener {
	/**
	 * mainFrame allows to check in/check out.
	 */
	private MainFrameTimeClock mainFrame;
	/**
	 * tabbedPane contains all tabs.
	 */
	private JTabbedPane tabbedPane;
	/**
	 * timeClock allows to have access of all information needed.
	 */
	private TimeClock timeClock;
	
	/**
	 * Constructor
	 * @param tc
	 */
	public WindowTimeClock(TimeClock tc) {
		timeClock = tc;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 300);
		setMinimumSize(new Dimension(470, 200));
		setTitle("Pointeuse");
		addWindowListener(this);
		
		tabbedPane = new JTabbedPane();
		
		
		mainFrame = new MainFrameTimeClock(timeClock);
		tabbedPane.addTab("Page d acceuil", mainFrame);
		
		new Thread(new TimeTimeClock(mainFrame)).start();
		
		
		TimeClockSettings timeClockSettings = new TimeClockSettings(timeClock);
		tabbedPane.addTab("Parametres", timeClockSettings);
		
		tabbedPane.addChangeListener(this);
		add(tabbedPane);
		
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	/**
	 * This method allows to save data when window is closed.
	 * @param arg0
	 */
	@Override
	public void windowClosing(WindowEvent arg0) {
		timeClock.saveData();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}
	
	/**
	 * This method allows to update chosen tab. It is called each time we change tab. 
	 * @param event
	 */
	@Override
	public void stateChanged(ChangeEvent event) {
		View view = (View) tabbedPane.getSelectedComponent();
		view.updateView();
	}
	
	/**
	 * This method returns mainframe attributes.
	 * @return mainFrame
	 */
	public MainFrameTimeClock getMainFrame() {
		return mainFrame;
	}
}