package src.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetSocketAddress;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import src.launcher.CentralApp;

/**
 * CentralAppSettings class contains all attributes to print tab to show and edit settings of CentralApp.
 */
public class CentralAppSettings extends JPanel implements ActionListener, View {
    
	/*
	 * Attributes
	 */
	
	/**
	 * centralApp allows to have access of all information needed.
	 */
	private CentralApp centralApp;
    /**
     * ipP allows to input TimeClock's IP address.
     */
	private JTextField ipP;
    /**
     * portP allows to input TimeClock's port.
     */
	private JTextField portP;
    /**
     *  portC allows to input CentralApp's port.
     */
	private JTextField portC;
    /**
     * advance allows to input earlyTime attribute in company.
     */
	private JTextField advance;
    /**
     * late allows to input lateTime attribute in company.
     */
	private JTextField late;
	/**
	 * labelAdvance indicates use of JTextField advance.
	 */
	private JLabel labelAdvance;
	/**
	 * labelLate indicates use of JTextField late.
	 */
	private JLabel labelLate;
	/**
	 * valider is button to confirm edition of settings.
	 */
	private JButton valider;
    
	/*
	 * Methods
	 */
	
	/**
 	* Constructor
 	* @param centralApp0
 	*/
	public CentralAppSettings(CentralApp centralApp0) {
		//start
 		 centralApp = centralApp0;
     	setBorder(new EmptyBorder(10, 10, 10, 10));
 		 setLayout(new GridLayout(1,2, 20, 0));
 		 JPanel leftPanel = new JPanel ();
 		 JPanel rightPanel = new JPanel ();
  		 leftPanel.setLayout(new GridLayout(6, 1, 0, 40));
  		 rightPanel.setLayout(new GridLayout(6,1, 0, 40));
  		 
  		 //left
  		 JLabel labelIPP= new JLabel("adresse IP de la pointeuse");
  		 JLabel labelPortP= new JLabel("port de la pointeuse");
  		 JLabel labelPortC= new JLabel("port de l application centrale");
  		 labelAdvance= new JLabel("departs en avance");
  		 labelLate= new JLabel("arrivees en retard");
  		 leftPanel.add(labelIPP);
  		 leftPanel.add(labelPortP);
  		 leftPanel.add(labelPortC);
  		 leftPanel.add(labelAdvance);
  		 leftPanel.add(labelLate);
  		 
  		 //right
  		 ipP = new JTextField();
  		 portP = new JTextField();
  		 portC = new JTextField();
  		 advance = new JTextField();
  		 late = new JTextField();
  		 valider =  new JButton("valider");
  		 valider.addActionListener(this);
  		 rightPanel.add(ipP);
  		 rightPanel.add(portP);
  		 rightPanel.add(portC);
  		 rightPanel.add(advance);
  		 rightPanel.add(late);
  		rightPanel.add(valider);
  		 
  		 add(leftPanel);
  		 add(rightPanel);
   	 
	}
    
    /**
     * This method overshadows actionPerformed method. Thanks to this method we can edit settings of CentralApp.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
    	
    	// test empty
   		if (ipP.equals("")) {
   			JOptionPane.showMessageDialog(this, "Le champ de l adresse IP de la pointeuse doit etre remplit");
   			return;
   		}
   		if (portC.equals("")) {
   			JOptionPane.showMessageDialog(this, "Le champ du port de l'application central doit être remplit");
   			return;
   		}
   		if (portP.equals("")) {
   			JOptionPane.showMessageDialog(this, "Le champ du port de la pointeuse doit être remplit");
   			return;
   		}
   		if (portP.equals("")) {
   			JOptionPane.showMessageDialog(this, "Le champ departs en avance doit être remplit");
   			return;
   		}
   		if (portP.equals("")) {
   			JOptionPane.showMessageDialog(this, "Le champ departs en retard doit être remplit");
   			return;
   		}
   		
   		// client person
   		String ip = ipP.getText().trim();
		String sPort = portP.getText().trim();
		try {
			int port = Integer.valueOf(sPort);
			InetSocketAddress isANew = new InetSocketAddress(ip, port);
			centralApp.getClientPerson().setIsA(isANew);
		} catch(NumberFormatException exc) {
			JOptionPane.showMessageDialog(this, "Le champ du port de la pointeuse n'est pas un nombre entier");
		} catch(IllegalArgumentException exc) {
			JOptionPane.showMessageDialog(this, "Le champ du port de la pointeuse n'est pas un port valide");
		}
   		
		// server pointing
		String sPort2 = portC.getText().trim();
		try {
			int port2 = Integer.valueOf(sPort2);
			String ip2 = centralApp.getServerPointing().getIsA().getHostName();
			InetSocketAddress isANew2 = new InetSocketAddress(ip2, port2);
			centralApp.getServerPointing().setIsA(isANew2);
		} catch(NumberFormatException exc) {
			JOptionPane.showMessageDialog(this, "Le champ du port de la pointeuse n'est pas un nombre entier");
		} catch(IllegalArgumentException exc) {
			JOptionPane.showMessageDialog(this, "Le champ du port de la pointeuse n'est pas un port valide");
		}

		// minutes advance
		 String adv = advance.getText().trim();
		 try {
			 int earlyTimeNew = Integer.valueOf(adv);
			 if (earlyTimeNew <= 0) {
				 JOptionPane.showMessageDialog(this, "Le champ " + labelAdvance.getText() +" doit être un nombre entier de minutes suppérieur à 0");
			 }
			 centralApp.getCompany().setEarlyTime(earlyTimeNew);
		} catch(NumberFormatException exc) {
			JOptionPane.showMessageDialog(this, "Le champ " + labelAdvance.getText() +" de la pointeuse n'est pas un nombre entier de minutes");
		}
		
		// minutes late
		 String sLate = late.getText().trim();
		 try {
			 int lateTimeNew = Integer.valueOf(sLate);
			 if (lateTimeNew <= 0) {
				 JOptionPane.showMessageDialog(this, "Le champ " + labelLate.getText() +" doit être un nombre entier de minutes suppérieur à 0");
			 }
			 centralApp.getCompany().setLateTime(lateTimeNew);
		} catch(NumberFormatException exc) {
			JOptionPane.showMessageDialog(this, "Le champ " + labelLate.getText() +" de la pointeuse n'est pas un nombre entier de minutes");
		}
    }

	/**
	 * This method allows to update information after manipulation on this tab.
	 */
	@Override
	public void updateView() {
		portP.setText(Integer.toString(centralApp.getClientPerson().getIsA().getPort()));
		ipP.setText(centralApp.getClientPerson().getIsA().getHostName());
		portC.setText(Integer.toString(centralApp.getServerPointing().getIsA().getPort()));
		advance.setText(Integer.toString(centralApp.getCompany().getEarlyTime()));
		late.setText(Integer.toString(centralApp.getCompany().getLateTime()));
	}
}