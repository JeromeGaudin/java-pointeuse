package src.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import src.launcher.CentralApp;
import src.model.Department;
import src.model.Employee;
import src.view.component.EmployeeTableModel;

/**
 * DepartmentView class contains all attributes to print tab to show and edit a Department. Thanks to this method we can see Department's employees.
 */
public class DepartmentView  extends JPanel implements ActionListener, View {
	/**
	 * centralApp allows to have access of all information needed.
	 */
	private CentralApp centralApp;
	/**
	 * tableLisEmp allows to see a list of all employees (and their information) in selected Department.
	 */
    private JTable tableLisEmp;
    /**
     * comboBoxDep contains all departments.
     */
    private JComboBox comboBoxDep;
    /**
     * comboBoxEmployees contains all employees.
     */
    private JComboBox comboBoxEmployees;
	/**
	 * validerModif is a button to confirm edition of selected Department.
	 */
    private JButton validerModif;
	/**
	 * validerA is a button to confirm addition of new Department.
	 */
    private JButton validerA;
	/**
	 * validerS is a button to confirm suppression of selected employee.
	 */
    private JButton validerS;
	/**
	 * supprimer is a button to confirm suppression of selected Department.
	 */
    private JButton supprimer;
    /**
     * id indicates where we can read selected Department's id.
     */
    private JLabel id;
    /**
     * name allows to input selected Department's new name to edit it's name.
     */
    private JTextField name;
    /**
     * nameNew allows to input new Department's name to create a new Department.
     */
    private JTextField nameNew;
    
    /**
     * Constructor
     * @param centralApp0
     */
    public DepartmentView(CentralApp centralApp0) {
   	 
		//start
		centralApp = centralApp0;
    	setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new GridLayout(1,2, 10, 0));
		JPanel leftPanel = new JPanel ();
		JPanel rightPanel = new JPanel ();
		leftPanel.setLayout(new GridLayout(4, 1, 0, 20));
		rightPanel.setLayout(new GridLayout(1,1));
		
		//left
		JPanel lPanel1 = new JPanel ();
		JPanel lPanel2 = new JPanel ();
		JPanel lPanel3 = new JPanel ();
		JPanel lPanel4 = new JPanel ();
		
		//1
		lPanel1.setLayout(new GridLayout(2, 2, 20, 10));
		lPanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Afficher"),
                BorderFactory.createEmptyBorder(10,10,10,10))
		);
		comboBoxDep = new JComboBox(centralApp.getCompany().getComboBoxModelDepartment());
		comboBoxDep.addActionListener(this);
		lPanel1.add(comboBoxDep);
		supprimer =  new JButton("supprimer");
		supprimer.addActionListener(this);
		lPanel1.add(supprimer);
		JLabel labelId = new JLabel("Identifiant");
		lPanel1.add(labelId);
		id = new JLabel("");
		lPanel1.add(id);//id
		 
		//2
		lPanel2.setLayout(new BorderLayout());
		lPanel2.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Changer le nom du département"),
                BorderFactory.createEmptyBorder(10,10,10,10))
		);
		name = new JTextField();
		lPanel2.add(name);
		validerModif =  new JButton("valider");
		validerModif.addActionListener(this);
		JPanel buttonPanel2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel2.add(validerModif);
		lPanel2.add(buttonPanel2, BorderLayout.SOUTH);

		
		//3
		lPanel3.setLayout(new BorderLayout());
		lPanel3.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Ajouter un département"),
                BorderFactory.createEmptyBorder(10,10,10,10))
		);
		nameNew = new JTextField();
		nameNew.setColumns(15);
		lPanel3.add(nameNew);
		validerA =  new JButton("valider");
		validerA.addActionListener(this);
		JPanel buttonPanel3 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel3.add(validerA);
		lPanel3.add(buttonPanel3, BorderLayout.SOUTH);
		
		
		
		//4
		lPanel4.setLayout(new BorderLayout());
		lPanel4.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Supprimer du département"),
                BorderFactory.createEmptyBorder(10,10,10,10))
		);
		comboBoxEmployees = new JComboBox();
		lPanel4.add(comboBoxEmployees);
		validerS =  new JButton("Valider");
		validerS.addActionListener(this);
		JPanel buttonPanel4 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel4.add(validerS);
		lPanel4.add(buttonPanel4, BorderLayout.SOUTH);
		 
		//right
		EmployeeTableModel model = new EmployeeTableModel(new ArrayList<Employee>(), centralApp.getCompany());
		tableLisEmp = new JTable(model); 
		rightPanel.add(new JScrollPane(tableLisEmp));
		
		//end
		leftPanel.add(lPanel1);
		leftPanel.add(lPanel2);
		leftPanel.add(lPanel3);
		leftPanel.add(lPanel4);
		add(leftPanel);
		add(rightPanel);
		
    }

    /**
     * This method overshadows actionPerformed method. Thanks to this method we can edit attributes of a Department.
     */
    @Override
  	public void actionPerformed(ActionEvent e) {
 		
      	if (e.getSource() == supprimer) {
      		if (comboBoxDep.getSelectedItem() != null) {
	      		int IdDepartment = ((Department) comboBoxDep.getSelectedItem()).getId();
	        	if (centralApp.getCompany().getAllEmpoyeesWithDepartment(IdDepartment).size() != 0) {
	        		JOptionPane.showMessageDialog(this, "Il faut enlever toutes les personnes du departement pour pouvoir supprimer le departement");
	        		
	           	} else {
	           		centralApp.getCompany().removeDepartment(IdDepartment);
	          		centralApp.getCompany().getComboBoxModelDepartment().removeElementAt(comboBoxDep.getSelectedIndex());
	          		centralApp.getWindow().getPointing().getDefaultComboBoxModelDepartment().removeElement(comboBoxDep.getSelectedIndex());
	           	}
      		}
      	} 
      	else if (e.getSource() == validerModif) {
      		if (comboBoxDep.getSelectedItem() != null) {
          		String sTmp = name.getText().trim();
      			if (sTmp.equals("")) {
      				JOptionPane.showMessageDialog(this, "Le champ du nom du departement au dessus doit etre remplit");
      			} else {
		      		int IdDepartment = ((Department) comboBoxDep.getSelectedItem()).getId();
		      		Department dep = centralApp.getCompany().getDepartment(IdDepartment);
		      		dep.setName(sTmp);
			      	
		      		updateUI(); // actualise les composant
			      	
		      		name.setText("");
      			}
      		}
      	}
      	else if (e.getSource() == validerA) {
      		String sTmp = nameNew.getText().trim();
      		if ( sTmp.contentEquals("")) {
      			JOptionPane.showMessageDialog(this, "Le champ du nom du departement au dessus doit etre remplit");
      		} else {
          		Department dep = new Department(sTmp);
          		centralApp.getCompany().addDepartment(dep);
          		
          		centralApp.getCompany().getComboBoxModelDepartment().addElement(dep);
          		centralApp.getWindow().getPointing().getDefaultComboBoxModelDepartment().addElement(dep);
          		
          		nameNew.setText("");
      		}
      	}
      	else if (e.getSource() == validerS) {
      		Employee emp = (Employee) comboBoxEmployees.getSelectedItem();
      		centralApp.getCompany().getEmployee(emp.getId()).setDepartment(null);
      		
      		comboBoxEmployees.removeItemAt(comboBoxEmployees.getSelectedIndex());
      		updateView();
      	}
      	else if (e.getSource() == comboBoxDep) {
      		if (comboBoxDep.getSelectedItem() != null) {
	      		int idDep = ((Department) comboBoxDep.getSelectedItem()).getId();
	      		
	      		id.setText(Integer.toString(idDep));
	      		
	      		List<Employee> listEmp = centralApp.getCompany().getAllEmpoyeesWithDepartment(idDep);
				EmployeeTableModel etm = (EmployeeTableModel) tableLisEmp.getModel();
				etm.setData(listEmp);
				etm.fireTableDataChanged(); // event 
				
				comboBoxEmployees.removeAllItems();
				for (Employee emp : listEmp) {
					comboBoxEmployees.addItem(emp);
				}
      		}
      	}
   }

	/**
	 * This method allows to update information after manipulation on this tab.
	 */
	@Override
	public void updateView() {
		// simulates select item on comboBoxDep JComboBox
		ActionEvent actionEvent = new ActionEvent(comboBoxDep, ActionEvent.ACTION_PERFORMED, "");
	    this.actionPerformed(actionEvent);
	}
}
