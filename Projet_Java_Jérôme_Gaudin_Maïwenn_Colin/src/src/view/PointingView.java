package src.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumnModel;

import src.launcher.CentralApp;
import src.model.Department;
import src.model.Employee;
import src.model.Pointing;
import src.view.component.LocalDateInput;
import src.view.component.PointingTableCellRenderer;
import src.view.component.PointingTableModel;

/**
 * PointingView class contains all attributes to print tab to show Pointings.
 */
public class PointingView extends JPanel implements ActionListener, View {
	/**
	 * centralApp allows to have access of all information needed.
	 */
	private CentralApp centralApp;
	/**
	 * buttonAllPointing is a button to show all Pointing.
	 */
	private JButton buttonAllPointing;
	/**
	 * buttonPointingDay is a button to show all Pointing today.
	 */
	private JButton buttonPointingDay;
	/**
	 * buttonPartialView is a button to show some Pointing. Pointing showed depend on chosen parameters.
	 */
	private JButton buttonPartialView;
	/**
     * comboBoxEmployee contains all employees.
     */
	private JComboBox comboBoxEmployee;
    /**
     * comboBoxDep contains all departments.
     */
	private JComboBox comboBoxDepartement;
	/**
	 * DateInputStartTime is the beginning of chosen period to show Pointing. If it's null, it starts with the older Pointing.
	 */
	private LocalDateInput DateInputStartTime;
	/**
	 * DateInputEndTime is the end of chosen period to show Pointing. If it's null it ends with the younger Pointing.
	 */
	private LocalDateInput DateInputEndTime;
	/**
	 * table allows to see a list of all selected Pointing (and their information).
	 */
	private JTable table;
	
	/**
	 * Constructor
	 * @param newCentralApp
	 */
	public PointingView(CentralApp newCentralApp) {
		centralApp = newCentralApp;
		
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new GridLayout(1, 2, 20, 0));
		
		JPanel panelLeft = new JPanel();
		JPanel panelRight = new JPanel(new BorderLayout());
		
		
		// panel left
		panelLeft.setLayout(new GridLayout(6, 1, 50, 30));
		
		// button all pointing
		buttonAllPointing = new JButton("Tous les pointages");
		buttonAllPointing.addActionListener(this);
		panelLeft.add(buttonAllPointing);
		
		// button pointing of the day
		buttonPointingDay = new JButton("Pointage de la journée");
		buttonPointingDay.addActionListener(this);
		panelLeft.add(buttonPointingDay);
		
		// JComboBox person 
		comboBoxEmployee = new JComboBox();
		comboBoxEmployee.addItem(null);
		for (Employee emp : centralApp.getCompany().getAllEmployee()) {
			comboBoxEmployee.addItem(emp);
		}
		panelLeft.add(comboBoxEmployee);
		
		// panel of period
		JPanel panelTime = new JPanel();
		panelTime.setLayout(new FlowLayout());
		
		// start time
		JLabel startTimeLabel = new JLabel("Periode du");
		panelTime.add(startTimeLabel);
		
		DateInputStartTime = new LocalDateInput();
		panelTime.add(DateInputStartTime);
		
		// end time
		JLabel endTimeLabel = new JLabel("au");
		panelTime.add(endTimeLabel);
		
		DateInputEndTime = new LocalDateInput();
		panelTime.add(DateInputEndTime);
		
		
		panelLeft.add(panelTime);
		
		
		// JComboBox Departement
		comboBoxDepartement = new JComboBox();
		comboBoxDepartement.addItem(null);
		for (Department dep : centralApp.getCompany().getDepartment()) {
			comboBoxDepartement.addItem(dep);
		}
		panelLeft.add(comboBoxDepartement);
		
		// button partialView
		buttonPartialView = new JButton("Voir");
		buttonPartialView.addActionListener(this);
		panelLeft.add(buttonPartialView);		
		
		// right panel 		
		PointingTableModel model = new PointingTableModel(new ArrayList<Pointing>());
		table = new JTable(model);
		panelRight.add(new JScrollPane(table), BorderLayout.CENTER);


		TableColumnModel tcm = table.getColumnModel();
		for (int i = 0; i < tcm.getColumnCount(); i++) {
			tcm.getColumn(i).setCellRenderer(new PointingTableCellRenderer(centralApp.getCompany()));
		}
		
		add(panelLeft);
		add(panelRight);
	}

	/**
     * This method overshadows actionPerformed method. Thanks to this method we can see all Pointing or just some oh them.
     */
	@Override
	public void actionPerformed(ActionEvent evenement) {
		// the button all pointing
		if (evenement.getSource() == buttonAllPointing) {
			List<Pointing> listPointing = centralApp.getCompany().searchPointing(null, null, null, null);
			
			PointingTableModel ptm = (PointingTableModel) table.getModel();
			ptm.setData(listPointing);
			ptm.fireTableDataChanged(); // event 
			
		}
		// the button pointing on day
		else if (evenement.getSource() == buttonPointingDay) {
			LocalDateTime start = LocalDate.now().atTime(0, 0, 0, 0);
			start = start.minusNanos(1);
			LocalDateTime end = start.plusDays(1);
			
			
			List<Pointing> listPointing = centralApp.getCompany().searchPointing(start, end, null, null);
			
			PointingTableModel ptm = (PointingTableModel) table.getModel();
			ptm.setData(listPointing);
			ptm.fireTableDataChanged();
		}
		// the button parital view
		else if (evenement.getSource() == buttonPartialView) {
			
			Employee employee = (Employee) comboBoxEmployee.getItemAt(comboBoxEmployee.getSelectedIndex());
			Department department = (Department) comboBoxDepartement.getItemAt(comboBoxDepartement.getSelectedIndex());
			
			LocalDate startDate = null, endDate = null;
			LocalDateTime startDateTime = null, endDateTime = null;
			try {
				startDate = DateInputStartTime.readLocalDate();
				if (startDate != null) {
					startDateTime = startDate.atTime(0, 0, 0, 0);
				}
			} catch(DateTimeException exception) {
				JOptionPane.showMessageDialog(this, "Ce jour indiqué dans la date du début n'existe pas");
				return;
			} catch (IllegalArgumentException exception) {
				JOptionPane.showMessageDialog(this, "Il faut remplire la date de début au complet");
				return;
			}

			try {
				endDate = DateInputEndTime.readLocalDate();
				if (endDate != null) {
					endDateTime = endDate.atTime(23, 59, 59, 999999999);
				}
			} catch(DateTimeException exception) {
				JOptionPane.showMessageDialog(this, "Ce jour indiqué dans la date de fin n'existe pas");
				return;
			} catch (IllegalArgumentException exception) {
				JOptionPane.showMessageDialog(this, "Il faut remplire la date de fin au complet");
				return;
			}
			
			if (startDateTime != null && endDateTime != null && startDateTime.isAfter(endDateTime)) {
				JOptionPane.showMessageDialog(this, "La date de debut doit etre avant la date de fin");
				return;
			}
	

			List<Pointing> listPointing = centralApp.getCompany().searchPointing(startDateTime, endDateTime, employee, department);
			
			PointingTableModel ptm = (PointingTableModel) table.getModel();
			ptm.setData(listPointing);
			ptm.fireTableDataChanged();
		}
	}
	
	public DefaultComboBoxModel getDefaultComboBoxModelDepartment() {
		return (DefaultComboBoxModel) comboBoxDepartement.getModel();
	}
	
	public DefaultComboBoxModel getDefaultComboBoxModelEmployee() {
		return (DefaultComboBoxModel) comboBoxEmployee.getModel();
	}

	/**
	 * This method allows to update information after manipulation on this tab.
	 */
	@Override
	public void updateView() {
		// simulates press on buttonAllPointing
		ActionEvent actionEvent = new ActionEvent(buttonAllPointing, ActionEvent.ACTION_PERFORMED, "");
	    this.actionPerformed(actionEvent);
	}
}