package src.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import src.launcher.TimeClock;
import src.model.Person;
import src.model.Pointing;

/**
 * MainFrameTimeClock class contains all attributes to print tab to show time and check in/check out.
 */
public class MainFrameTimeClock extends JPanel implements ActionListener, View {
	/**
	 * checkButton is a button to check in or check out.
	 */
	private JButton checkButton;
	/**
	 * timeClock allows to have access of all information needed.
	 */
	private TimeClock timeClock;
	/**
     * comboBoxPeople contains all employees.
     */
	private JComboBox comboBoxPeople;
	/**
	 * synchronizeButton is a button to serialize and send data to CentralApp.
	 */
	private JButton synchronizeButton;
	/**
	 * time allows to indicate time just now.
	 */
	private JLabel time;
		
	/**
	 * Constructor 
	 * @param timeClock0 
	 */
	public MainFrameTimeClock(TimeClock timeClock0) {
		timeClock = timeClock0;
		
    	setBorder(new EmptyBorder(10, 10, 10, 10));
    	setLayout(new BorderLayout());
		
		time = new JLabel();
		add(time, BorderLayout.CENTER);
		time.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(2, 1, 0, 20));
		
		synchronizeButton = new JButton("synchronize");
		synchronizeButton.addActionListener(this);
		JPanel flowPanel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		flowPanel1.add(synchronizeButton);
		bottomPanel.add(flowPanel1);
		
		
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
		
		comboBoxPeople = new JComboBox(timeClock.getTimeClockData().getDefaultComboBoxModelPeople());
		buttonPanel.add(comboBoxPeople);
		
		checkButton = new JButton("Check in/out");
		checkButton.addActionListener(this);
		buttonPanel.add(checkButton);
		
		bottomPanel.add(buttonPanel);
		add(bottomPanel, BorderLayout.SOUTH);
	}

    /**
     * This method overshadows actionPerformed method. Thanks to this method we can synchronize data or check in/check out.
     */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == checkButton) {
			if (comboBoxPeople.getSelectedItem() != null) {
				Person person = (Person) comboBoxPeople.getItemAt(comboBoxPeople.getSelectedIndex());
				
				List<Pointing> pointingList = timeClock.getTimeClockData().getPointing();
				Pointing pointing = new Pointing(person);
				if ( !pointingList.contains(pointing)) {
					pointingList.add(pointing);
				}
			}
		}
		else if (e.getSource() == synchronizeButton) {
			timeClock.synchronization();
		}
	}
	
	/**
	 * This method allows to edit time attribute.
	 * @param newTime
	 */
	public void setTime(String newTime) {
		time.setText(newTime);
	}
	
	/**
	 * This method allows to update information after manipulation on this tab.
	 */
	@Override
	public void updateView() {
		// do nothing
	}
}