package src.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import src.launcher.TimeClock;

/**
 * TimeClockSettings class contains all attributes to print tab to show and edit settings of TimeClock.
 */
public class TimeClockSettings extends JPanel implements ActionListener, View {
	/**
	 * timeClock allows to have access of all information needed.
	 */
	private TimeClock timeClock;
    /**
     * ipC allows to input CentralApp IP address.
     */
    private JTextField ipC;
    /**
     *  portC allows to input CentralApp's port.
     */
	private JTextField portC;
    /**
     * portP allows to input TimeClock's port.
     */
	private JTextField portP;
	/**
	 * valider is a button to confirm edition of settings.
	 */
	private JButton valider;
    
	/**
 	* Constructor
 	* @param centralApp0
 	*/
	public TimeClockSettings(TimeClock tc) {
		timeClock = tc;
		//start
    	setBorder(new EmptyBorder(10, 10, 10, 10));
 		setLayout(new GridLayout(1,2, 10, 0));
 		JPanel leftPanel = new JPanel ();
 		JPanel rightPanel = new JPanel ();
  		leftPanel.setLayout(new GridLayout(4, 1, 0, 20));
  		rightPanel.setLayout(new GridLayout(4, 1, 0, 20));
  		 
  		 //left
  		JLabel labelIPC= new JLabel("adresse IP de la centrale");
  		JLabel labelPortC= new JLabel("port de la centrale");
  		JLabel labelPortP= new JLabel("port de l application pointeuse");
  		leftPanel.add(labelIPC);
  		leftPanel.add(labelPortC);
  		leftPanel.add(labelPortP);
  		
  		//right
  		ipC = new JTextField();
  		portP = new JTextField();
  		portC = new JTextField();
  		valider =  new JButton("valider");
  		valider.addActionListener(this);
  		rightPanel.add(ipC);
  		rightPanel.add(portC);
  		rightPanel.add(portP);
  		rightPanel.add(valider);
  		 
  		add(leftPanel);
  		add(rightPanel); 
	}
	
	/**
     * This method overshadows actionPerformed method. Thanks to this method we can see all settings of TimeClock.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
	   	if (e.getSource() == valider) {
	   		
	   		// verification
	   		if (ipC.equals("")) {
	   			JOptionPane.showMessageDialog(this, "Le champ de l'adresse IP de l'application central doit être remplit");
	   			return;
	   		}
	   		if (portC.equals("")) {
	   			JOptionPane.showMessageDialog(this, "Le champ du port de l'application central doit être remplit");
	   			return;
	   		}
	   		if (portP.equals("")) {
	   			JOptionPane.showMessageDialog(this, "Le champ du port de la pointeuse doit être remplit");
	   			return;
	   		}
	   		
	   		// client pointing
	   		String ip = ipC.getText().trim();
   			String sPort = portC.getText().trim();
   			try {
   				int port = Integer.valueOf(sPort);
				InetSocketAddress isANew = new InetSocketAddress(ip, port);
				timeClock.getClientPointing().setIsA(isANew);
   			} catch(NumberFormatException exc) {
				JOptionPane.showMessageDialog(this, "Le champ du port de l'application centrale n'est pas un nombre entier");
			} catch(IllegalArgumentException exc) {
				JOptionPane.showMessageDialog(this, "Le champ du port de l'application centrale n'est pas un port valide");
			}
   			
			// server person
			String sPort2 = portP.getText().trim();
			try {
				int port2 = Integer.valueOf(sPort2);
				String ip2 = timeClock.getServerPerson().getIsA().getHostName();
				InetSocketAddress isANew2 = new InetSocketAddress(ip2, port2);
				timeClock.getServerPerson().setIsA(isANew2);
			} catch(NumberFormatException exc) {
				JOptionPane.showMessageDialog(this, "Le champ du port de la pointeuse n'est pas un nombre entier");
			} catch(IllegalArgumentException exc) {
				JOptionPane.showMessageDialog(this, "Le champ du port de la pointeuse n'est pas un port valide");
			}

			/*try {
				Runtime.getRuntime().exec("java -jar centralApp.jar");
			} catch (IOException e1) {
				//TODO
			}
			System.exit(0);*/

			updateView();
	   	}
    }
    
    /**
	 * This method allows to update information after manipulation on this tab.
	 */
	@Override
	public void updateView() {
		portC.setText(Integer.toString(timeClock.getClientPointing().getIsA().getPort()));
		ipC.setText(timeClock.getClientPointing().getIsA().getHostName());
		portP.setText(Integer.toString(timeClock.getServerPerson().getIsA().getPort()));
	}
}


