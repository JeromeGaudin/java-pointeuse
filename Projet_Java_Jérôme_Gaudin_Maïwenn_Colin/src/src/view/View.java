package src.view;

/**
 * This interface allows to update tab. All view implement this interface.
 */
public interface View {
    /**
	 * This method allows to update information after manipulation on tabs.
	 */
	public void updateView();
}
