package src.view.component;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import src.model.Company;
import src.model.Employee;

/**
 * EmployeeTableModel contains Employee.
 */
public class EmployeeTableModel extends AbstractTableModel {
	private List<Employee> employee;
	private Company company;
	
	/**
	 * Constructor
	 * @param emp
	 * @param comp
	 */
	public EmployeeTableModel(List<Employee> emp, Company comp) {
		employee = emp;
		company = comp;
	}
	
	/**
	 * This method allows to edit employee attribute.
	 * @param emp
	 */
	public void setData(List<Employee> emp) {
		employee = emp;
	}

	/**
	 * This method returns the number of column.
	 */
	@Override
	public int getColumnCount() {
		return 3;
	}

	/**
	 * This method returns the number of row.
	 */
	@Override
	public int getRowCount() {
		return employee.size();
	}
	
	/**
	 * This method returns the name of column.
	 * @param columnIndex
	 */
    @Override
    public String getColumnName(int columnIndex) {
    	switch (columnIndex) {
    	case 0:
    		return "Prenom";
    	case 1:
    		return "Nom";
    	case 2:
    		return "Manager";
    	default:
    		return null;
    	}
    }

    /**
     * This method returns value in tab.
	 * @param columnIndex
	 * @param rowIndex
     */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Employee emp = employee.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			return emp.getFirstName();
		case 1:
			return emp.getLastName();
		case 2:
			return company.isManager(emp.getId());
		default :
			return null;
		}
	}
	
	/**
	 * This method returns class of attributes.
	 * @param columnIndex
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
        case 1:
        	return String.class;
        case 2:
        	return Boolean.class;
    	default:
    		return String.class;
        }
	}
}