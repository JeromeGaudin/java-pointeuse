package src.view.component;

import java.time.DateTimeException;
import java.time.LocalDate;

import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * This class allows to enter a date.
 */
public class LocalDateInput extends JPanel {
	/**
	 * DAY is to answer about day.
	 */
	private static final Integer[] DAY = new Integer[]{null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
	/**
	 * MONTH is to answer about month.
	 */
	private static final Integer[] MONTH = new Integer[]{null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
	/**
	 * YEAR is to answer about year.
	 */
	private static final Integer[] YEAR = new Integer[] {null, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031, 2032};
	/**
	 * comboBoxDay contains all days possible.
	 */
	private JComboBox<Integer> comboBoxDay;
	/**
	 * comboBoxMonth contains all month possible.
	 */
	private JComboBox<Integer> comboBoxMonth;
	/**
	 * comboBoxYear contains all year possible.
	 */
	private JComboBox<Integer> comboBoxYear;
	
	/**
	 * Default constructor
	 */
	public LocalDateInput() {
		comboBoxDay = new JComboBox<Integer>(DAY);
		comboBoxMonth = new JComboBox<Integer>(MONTH);
		comboBoxYear = new JComboBox<Integer>(YEAR);
		
		add(comboBoxDay);
		add(comboBoxMonth);
		add(comboBoxYear);
	}
	
	/**
	 * This method allows to read the chosen date.
	 * @return LocalDate
	 * @throws DateTimeException
	 * @throws IllegalArgumentException
	 */
	public LocalDate readLocalDate() throws DateTimeException, IllegalArgumentException {
		Integer day = (Integer) comboBoxDay.getSelectedItem();
		Integer month = (Integer) comboBoxMonth.getSelectedItem();
		Integer year = (Integer) comboBoxYear.getSelectedItem();
		
		if (day == null && month == null && year == null) {
			return null;
		} else if (day == null || month == null || year == null) {
			throw new IllegalArgumentException("La date n'est pas complète");
		}
		
		LocalDate date = LocalDate.of(year, month, day);
		return date;	
	}
}
