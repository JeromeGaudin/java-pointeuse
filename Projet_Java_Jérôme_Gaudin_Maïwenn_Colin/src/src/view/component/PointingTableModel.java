package src.view.component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import src.model.Pointing;

/**
 * This class contain all method to represent a Pointing in a JTable.
 */
public class PointingTableModel extends AbstractTableModel {
	/*
	 * Attributes
	 */
	
	/**
	 * pointings a List of pointing to print on a JTable
	 */
	private List<Pointing> pointings;
	
	// setters
	/**
	 * This method allows to edit pointings.
	 * @param pointing
	 */
	public void setData(List<Pointing> pointing) {
		pointings = pointing;
	}
	
	// getters
	/**
	 * This method returns the pointing corresponding to the line number.
	 * @param index
	 * @return Pointing
	 */
	public Pointing getItemAt(int index) {
		return pointings.get(index);
	}
	
	/**
	 * This method return the number of column.
	 * @return int
	 */
	@Override
	public int getColumnCount() {
		return 4;
	}
	
	/**
	 * This method returns the number of row.
	 * @return int
	 */
	@Override
	public int getRowCount() {
		return pointings.size();
	}
	
	/**
	 * This method returns the name of the column according to columnIndex.
	 * @param columnIndex
	 * @return String
	 */
    @Override
    public String getColumnName(int columnIndex) {
    	switch (columnIndex) {
    	case 0:
    		return "Prenom";
    	case 1:
    		return "Nom";
    	case 2:
    		return "Heure";
    	case 3:
    		return "Date";
    	default:
    		return null;
    	}
    }

    /**
     * This method returns value of rowIndex and columnIndex.
     * @param rowIndex
     * @param columnIndex
     * @return Object
     */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Pointing pointing = pointings.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			return pointing.getPerson().getFirstName();
		case 1:
			return pointing.getPerson().getLastName();
		case 2:
			return pointing.getTime().toLocalTime();
		case 3:
			return pointing.getTime().toLocalDate();
		default :
			return null;
		}
	}
	
	/**
	 * This method returns the class of the columnIndex.
	 * @param columnIndex
	 * @return Class<?>
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
        case 1:
        	return String.class;
        case 2:
        	return LocalTime.class;
        case 3: 
        	return LocalDate.class;
    	default:
    		return String.class;
        }
	}
	
	/*
	 * Other Methods
	 */
	
	/**
	 * Constructor
	 * @param pointing
	 */
	public PointingTableModel(List<Pointing> pointing) {
		pointings = pointing;
	}
}