package src.view.component;

import java.time.LocalTime;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class allows to enter an hour.
 */
public class LocalTimeInput extends JPanel {
	/**
	 * HOUR is to answer about the number of hour.
	 */
	private static final Integer[] HOUR = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23};
	/**
	 * MINUTE is to answer about the number of minutes.
	 */
	private static final Integer[] MINUTE = new Integer[]{0, 15, 30, 45};
	/**
	 * comboBoxHour contains all hours possible.
	 */
	private JComboBox<Integer> comboBoxHour;
	/**
	 * comboBoxMinute contains all minutes possible.
	 */
	private JComboBox<Integer> comboBoxMinute;
	
	/**
	 * Default constructor
	 */
	public LocalTimeInput() {
		comboBoxHour = new JComboBox<Integer>(HOUR);
		comboBoxMinute = new JComboBox<Integer>(MINUTE);
		JLabel doubleDot = new JLabel(":");

		add(comboBoxHour);
		add(doubleDot);
		add(comboBoxMinute);
	}
	
	/**
	 * This method allows to read the chosen hour.
	 * @return LocalTime
	 */
	public LocalTime readLocalTime() {
		Integer hour = (Integer) comboBoxHour.getSelectedItem();
		Integer minute = (Integer) comboBoxMinute.getSelectedItem();
		
		return LocalTime.of(hour, minute, 0, 0);
	}
	
	/**
	 * This method allows to write time.
	 * @param time
	 */
	public void writeLocalTime(LocalTime time) {
		for (int i = 0; i < HOUR.length ; i++) {
			if (time.getHour() == HOUR[i]) {
				comboBoxHour.setSelectedIndex(i);
				break;
			}
		}
		for (int i = 0; i < MINUTE.length ; i++) {
			if (time.getMinute() == MINUTE[i]) {
				comboBoxMinute.setSelectedIndex(i);
				break;
			}
		}
	}
}
