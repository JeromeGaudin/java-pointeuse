package src.view.component;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import src.model.Company;
import src.model.Pointing;
import src.model.WorkingDay;

/**
 * This class contains all methods for color pointing if it is after arrival or if it is before departure of a DaySchedule.
 */
public class PointingTableCellRenderer extends DefaultTableCellRenderer {
	/**
	 * colorLate is the color of late pointing (pointing after arrival).
	 */
	private Color colorLate;
	/**
	 * colorEarly is the color of early pointing (pointing before departure).
	 */
	private Color colorEarly;
	
	/**
	 * company contain all information.
	 */
	private Company company;
	
	/**
	 * Constructor
	 * @param comp
	 */
	public PointingTableCellRenderer(Company comp) {
		colorLate = Color.RED;
		colorEarly = Color.RED;
		company = comp;
	}
	
	/**
	 * This method is overshadow, It paint cell of JTable if the pointing are after arrival or if pointing are before departure.
	 * @param table where we want apply this method
	 * @param Object value the value 
	 * @param boolean isSelected if element are selected
	 * @param boolean hasFocus if it's focus
	 * @param int row the row
	 * @param int column the column
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		PointingTableModel model = (PointingTableModel) table.getModel();
		List<Pointing> list = company.showLateArrival();
		
		boolean found = false;
		for (Pointing pointing : list) {
			if (pointing == model.getItemAt(row)) {
				component.setBackground(colorLate);
				found = true;
				break;
			}
		}
		if ( !found) {
			list= company.showAdvanceDeparture();
			found = false;
			for (Pointing pointing : list) {
				if (pointing == model.getItemAt(row)) {
					component.setBackground(colorEarly);
					found = true;
					break;
				}
			}
		}
		if ( !found) {
			component.setBackground(Color.WHITE);
		}
		return component;
	}
}
