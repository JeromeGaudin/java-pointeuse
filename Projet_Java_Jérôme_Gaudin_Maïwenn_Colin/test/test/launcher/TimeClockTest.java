package test.launcher;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import src.launcher.TimeClock;
import src.model.Person;
import src.model.Pointing;

public class TimeClockTest {
	private static TimeClock timeClock;
	private static String fileNameTest = "testBackupFileTimeClock";
	
	@BeforeClass
	public static void initilize() {
		TimeClock.setBackupFileName(fileNameTest);
		timeClock = new TimeClock();
	}

	@Test
	public void saveData_restoreData() {

		Person person = new Person("margaux","Legrand");
		Person person2 = new Person("Alice","Chinon");
		Person person3 = new Person("Etienne","Dudard");

		timeClock.getTimeClockData().getDefaultComboBoxModelPeople().addElement(person);
		timeClock.getTimeClockData().getDefaultComboBoxModelPeople().addElement(person2);
		timeClock.getTimeClockData().getDefaultComboBoxModelPeople().addElement(person3);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 8, 16);
		Pointing p1 = new Pointing(person, t2);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 8, 25);
		Pointing p2 = new Pointing(person2, t3);
		LocalDateTime t4 = LocalDateTime.of(2019, 3, 21, 8, 35);
		Pointing p3 = new Pointing(person3, t4);
		LocalDateTime t5 = LocalDateTime.of(2019, 3, 21, 18, 16);
		Pointing p4 = new Pointing(person, t5);
		LocalDateTime t6 = LocalDateTime.of(2019, 3, 21, 18, 25);
		Pointing p5 = new Pointing(person2, t6);
		LocalDateTime t7 = LocalDateTime.of(2019, 3, 21, 18, 35);
		Pointing p6 = new Pointing(person3, t7);

		timeClock.getTimeClockData().getPointing().add(p1);
		timeClock.getTimeClockData().getPointing().add(p2);
		timeClock.getTimeClockData().getPointing().add(p3);
		timeClock.getTimeClockData().getPointing().add(p4);
		timeClock.getTimeClockData().getPointing().add(p5);
		timeClock.getTimeClockData().getPointing().add(p6);
		
		List<Pointing> list1 = new ArrayList<Pointing>(timeClock.getTimeClockData().getPointing());
		
		timeClock.saveData();
		
		timeClock.getTimeClockData().getPointing().removeAll(timeClock.getTimeClockData().getPointing());
		
		timeClock.restoreData();
    	
		List<Pointing> list2 = timeClock.getTimeClockData().getPointing();
		
		assertEquals(list1,list2);
	}
	
	@Test
	public void synchronization() {

	}
	
	@After
	public void deleteFile() {
		File file = new File(fileNameTest);
		file.delete();
	}
}