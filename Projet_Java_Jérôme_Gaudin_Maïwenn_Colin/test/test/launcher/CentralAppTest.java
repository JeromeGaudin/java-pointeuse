package test.launcher;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import src.launcher.CentralApp;
import src.model.DaySchedule;
import src.model.Department;
import src.model.Employee;
import src.model.Manager;
import src.model.Pointing;

public class CentralAppTest {
	private static CentralApp centralApp;
	private static String fileNameTest = "testBackupFileCentralApp";
	
	@BeforeClass
	public static void initilize() {
		CentralApp.setBackupFileName(fileNameTest);
		centralApp = new CentralApp();
	}
	
	@Test
	public void saveData_restoreData() {
		Employee emp = new Employee("margaux","Legrand");
		Manager emp2 = new Manager();
		emp2.setFirstName("Alice");
		emp2.setLastName("Chinon");
		Employee emp3 = new Employee("Etienne","Dudard");
		
		Department dep = new Department("Informatique");
		centralApp.getCompany().addDepartment(dep);
		Department dep2 = new Department("Electronique");
		centralApp.getCompany().addDepartment(dep2);
		emp.setDepartment(dep);
		emp2.setDepartment(dep);
		emp3.setDepartment(dep2);
		
		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		emp2.setSchedule(ds);
		emp3.setSchedule(ds);
		
		centralApp.getCompany().addEmployee(emp);
		centralApp.getCompany().addEmployee(emp2);
		centralApp.getCompany().addEmployee(emp3);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 8, 16);
		Pointing p1 = new Pointing(emp, t2);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 8, 25);
		Pointing p2 = new Pointing(emp2, t3);
		LocalDateTime t4 = LocalDateTime.of(2019, 3, 21, 8, 35);
		Pointing p3 = new Pointing(emp3, t4);
		LocalDateTime t5 = LocalDateTime.of(2019, 3, 21, 18, 16);
		Pointing p4 = new Pointing(emp, t5);
		LocalDateTime t6 = LocalDateTime.of(2019, 3, 21, 18, 25);
		Pointing p5 = new Pointing(emp2, t6);
		LocalDateTime t7 = LocalDateTime.of(2019, 3, 21, 18, 35);
		Pointing p6 = new Pointing(emp3, t7);
		
		centralApp.getCompany().addPointing(p1);
		centralApp.getCompany().addPointing(p2);
		centralApp.getCompany().addPointing(p3);
		centralApp.getCompany().addPointing(p4);
		centralApp.getCompany().addPointing(p5);
		centralApp.getCompany().addPointing(p6);
		
		centralApp.getCompany().setEarlyTime(31);
		centralApp.getCompany().setLateTime(46);
		
		
		List<Department> departments1 = new ArrayList<Department>(centralApp.getCompany().getDepartment());
		List<Employee> employees1 = new ArrayList<Employee>(centralApp.getCompany().getEmployee());
		List<Manager> managers1 = new ArrayList<Manager>(centralApp.getCompany().getManager());
		int earlyTime1 = centralApp.getCompany().getLateTime();
		int lateTime1 = centralApp.getCompany().getEarlyTime();
		
		
		centralApp.saveData();
		
		// delete all element
		centralApp.getCompany().getEmployee().removeAll(centralApp.getCompany().getEmployee());
		centralApp.getCompany().getDepartment().removeAll(centralApp.getCompany().getDepartment());
		centralApp.getCompany().getManager().removeAll(centralApp.getCompany().getManager());
		
		centralApp.restoreData();
		
		assertEquals(earlyTime1, centralApp.getCompany().getLateTime());
		assertEquals(lateTime1, centralApp.getCompany().getEarlyTime());
		
		List<Department> departments2 = new ArrayList<Department>(centralApp.getCompany().getDepartment());
		List<Employee> employees2 = new  ArrayList<Employee>(centralApp.getCompany().getEmployee());
		List<Manager> managers2 = new  ArrayList<Manager>(centralApp.getCompany().getManager());
		
		Collections.sort(departments1);
		Collections.sort(departments2);
		Collections.sort(employees1);
		Collections.sort(employees2);
		Collections.sort(managers1);
		Collections.sort(managers2);
		
		assertEquals(departments1,departments2);
		assertEquals(employees1,employees2);
		assertEquals(managers1,managers2);
	}
	
	@After
	public void deleteFile() {
		File file = new File(fileNameTest);
		file.delete();
	}
	
}