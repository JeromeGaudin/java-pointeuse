package test.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import src.model.Company;
import src.model.DaySchedule;
import src.model.Employee;
import src.model.Pointing;

public class EmployeeTest {
	private Employee emp;
	
	@Before
	public void initilize() {
		emp = new Employee("Alice", "Chinon");
	}
	
	@Test
	public void getOverTime() {
		assertEquals(emp.getOverTime(), 0.0, 0.01);
	}
	
	@Test
	public void	getCurrentDay() {
		assertEquals(emp.getCurrentDay(), false);
	}
	
	@Test
	public void setCurrentDay() {
		emp.setCurrentDay(true);
		assertEquals(emp.getCurrentDay(), true);
	}
	
	@Test
	public void default_constructor() {
		emp = new Employee();
		assertEquals(emp.getOverTime(), 0.0, 0.01);
		assertEquals(emp.getCurrentDay(), false);
		assertEquals(emp.getDepartment(), null);
		assertEquals(emp.getSchedule(), null);
	}
	
	@Test
	public void getAllPointing() {
		Employee emp = new Employee();
		
		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 8, 16);
		Pointing p1 = new Pointing(emp, t2);
		LocalDateTime t3 = LocalDateTime.of(2019, 4, 21, 8, 25);
		Pointing p2 = new Pointing(emp, t3);
		LocalDateTime t4 = LocalDateTime.of(2019, 5, 21, 8, 35);
		Pointing p3 = new Pointing(emp, t4);
		LocalDateTime t5 = LocalDateTime.of(2019, 3, 21, 18, 16);
		Pointing p4 = new Pointing(emp, t5);
		LocalDateTime t6 = LocalDateTime.of(2019, 4, 21, 18, 25);
		Pointing p5 = new Pointing(emp, t6);
		LocalDateTime t7 = LocalDateTime.of(2019, 5, 21, 18, 35);
		Pointing p6 = new Pointing(emp, t7);
		
		Company company = new Company();
		company.addEmployee(emp);
		
		company.addPointing(p1);
		company.addPointing(p2);
		company.addPointing(p3);
		company.addPointing(p4);
		company.addPointing(p5);
		company.addPointing(p6);
		
		List<Pointing> pointings = new ArrayList<Pointing>();
		pointings.add(p1);
		pointings.add(p2);
		pointings.add(p3);
		pointings.add(p4);
		pointings.add(p5);
		pointings.add(p6);
		List<Pointing> pointingTest = emp.getAllPointing();
		Collections.sort(pointings);
		Collections.sort(pointingTest);
		
		assertEquals(pointings,pointingTest);
	}
	
	@Test
	public void updateOverTime() {
		Employee emp = new Employee();

		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		
		Company company = new Company();
		company.addEmployee(emp);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 7, 45);
		Pointing p1 = new Pointing(emp, t2);
		company.addPointing(p1);
		
		assertEquals(emp.getOverTime(),0.5,0.01);
	}
	
	@Test
	public void containsPointing() {
		Employee emp = new Employee();

		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		
		Company company = new Company();
		company.addEmployee(emp);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 7, 45);
		Pointing p1 = new Pointing(emp, t2);
		company.addPointing(p1);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 18, 16);
		Pointing p2 = new Pointing(emp, t3);
		company.addPointing(p2);
		
		assertEquals(emp.containsPointing(p2),true);
	}
	
	@Test
	public void containsPointing2() {
		Employee emp = new Employee();

		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		
		Company company = new Company();
		company.addEmployee(emp);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 7, 45);
		Pointing p1 = new Pointing(emp, t2);
		company.addPointing(p1);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 18, 16);
		Pointing p2 = new Pointing(emp, t3);
		company.addPointing(p2);
		LocalDateTime t4 = LocalDateTime.of(2019, 5, 21, 18, 35);
		Pointing p3 = new Pointing(emp, t4);
		
		assertEquals(emp.containsPointing(p3),false);
	}
}