package test.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import src.model.Person;
import src.model.Pointing;

public class PointingTest {
	private Pointing pointing;
	private Person person;
	
	@Before
	public void initilize() {
		person = new Person();
		pointing = new Pointing(person);
	}
	
	@Test
	public void getTime() {
		LocalDateTime time1 = LocalDateTime.now();
    	time1 = Pointing.round(time1);
    	assertEquals(pointing.getTime(), time1);
	}
	
	@Test
	public void constructor() {
		person = new Person();
		pointing = new Pointing(person);
		LocalDateTime time1 = LocalDateTime.now();
    	time1 = Pointing.round(time1);
    	assertEquals(pointing.getTime(), time1);
	}
	
	@Test
	public void constructor2() {
		person = new Person();
		LocalDateTime time0 = LocalDateTime.of(2019,3, 12, 8,7);
		pointing = new Pointing(person, time0);
		LocalDateTime time1 = LocalDateTime.of(2019,3, 12, 8,0);
    	assertEquals(pointing.getTime(), time1);
	}
	
	@Test
	public void round() {
		LocalDateTime time1 = LocalDateTime.of(2019,3, 12, 8,8);
    	time1 = Pointing.round(time1);
    	assertEquals(time1,LocalDateTime.of(2019,3, 12, 8,15));
	}
	
	@Test
	public void toStringTest() {
		String str1 = pointing.toString();
		String str2 = person.toString() + " "+ pointing.getTime().toString();
		assertEquals(str1,str2);
	}
}