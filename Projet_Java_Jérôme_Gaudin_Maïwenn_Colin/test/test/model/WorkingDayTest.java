package test.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import src.model.Person;
import src.model.Pointing;
import src.model.WorkingDay;

public class WorkingDayTest {
	private WorkingDay wd;
	private Pointing pointing;
	private Pointing pointing2;
	private Person person;
	
	@Before
	public void initilize() {
		person = new Person();
		LocalDateTime time0 = LocalDateTime.of(2019,3, 12, 8,7);
		pointing = new Pointing(person, time0);
		wd = new WorkingDay(pointing);
	}
	
	@Test
	public void getArrival() {
		assertEquals(wd.getArrival(), pointing);
	}
	
	@Test
	public void getDeparture() {
		assertEquals(wd.getDeparture(), null);
	}
	
	@Test
	public void endWorkingDay() {
		LocalDateTime time1 = LocalDateTime.of(2019,3, 12, 9,7);
		pointing2 = new Pointing (person, time1);
		wd.endWorkingDay(pointing2);
		assertEquals(wd.getDeparture(), pointing2);
	}
	
	@Test
	public void constructor() {
		assertEquals(wd.getArrival(), pointing);
		assertEquals(wd.getDeparture(), null);
	}
}