package test.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import src.model.Company;
import src.model.DaySchedule;
import src.model.Department;
import src.model.Employee;
import src.model.Manager;
import src.model.Pointing;

public class CompanyTest {
	private Company company;
	
	@Before
	public void initilize() {
		company = new Company();
	}
	
	@Test
	public void getLateTime() {
		assertEquals(company.getLateTime(),15);
	}
	
	@Test
	public void getEarlyTime() {
		assertEquals(company.getEarlyTime(),15);
	}
	
	@Test
	public void setLateTime() {
		company.setLateTime(30);
		assertEquals(company.getLateTime(),30);
	}
	
	@Test
	public void setEarlyTime() {
		company.setEarlyTime(30);
		assertEquals(company.getEarlyTime(),30);
	}
	
	@Test
	public void getEmployee() {
		assertEquals(company.getEmployee().size(),0);
	}
	
	@Test
	public void getDepartment() {
		assertEquals(company.getDepartment().size(),0);
	}
	
	@Test
	public void addEmployee() {
		Employee emp = new Employee();
		company.addEmployee(emp);
		assertEquals(company.getEmployee().size(),1);
	}
	
	@Test
	public void addManager() {
		Manager man = new Manager();
		company.addManager(man);
		assertEquals(company.getAllEmployee().size(),1);
	}
	
	@Test
	public void getAllEmployee() {
		Employee emp = new Employee();
		company.addEmployee(emp);
		Manager man = new Manager();
		company.addManager(man);
		assertEquals(company.getAllEmployee().size(),2);
	}
	
	@Test
	public void addDepartment() {
		Department dep = new Department();
		company.addDepartment(dep);
		assertEquals(company.getDepartment().size(),1);
	}
	
	@Test
	public void removedEmployee() {
		Employee emp = new Employee();
		company.addEmployee(emp);
		Employee emp2 = new Employee();
		company.addEmployee(emp2);
		company.removeEmployee(emp.getId());
		assertEquals(company.getEmployee().size(),1);
	}
	
	@Test
	public void removeManager() {
		Manager man = new Manager();
		company.addManager(man);
		Manager man2 = new Manager();
		company.addManager(man2);
		company.removeManager(man.getId());
		assertEquals(company.getAllEmployee().size(),1);
	}
	
	@Test
	public void removeDepartment() {
		Department dep = new Department();
		company.addDepartment(dep);
		Department dep2 = new Department();
		company.addDepartment(dep2);
		company.removeDepartment(dep.getId());
		assertEquals(company.getDepartment().size(),1);
	}
	
	@Test
	public void addPointing() {
		Employee emp = new Employee();
		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		company.addEmployee(emp);
		Pointing p = new Pointing(emp);
		company.addPointing(p);
		
		assertEquals(company.getEmployee(emp.getId()).getCurrentDay(),true);
	}
	
	@Test
	public void showLateArrival() {
		Employee emp = new Employee();
		Employee emp2 = new Employee();
		company.addEmployee(emp);
		company.addEmployee(emp2);
		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		emp2.setSchedule(ds);
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 8, 16);
		Pointing p1 = new Pointing(emp, t2);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 8, 40);
		Pointing p2 = new Pointing(emp2, t3);
		company.addPointing(p1);
		company.addPointing(p2);
		
		List<Pointing> pointingLate = new ArrayList<Pointing>();
		pointingLate.add(p2);
		assertEquals(company.showLateArrival(),pointingLate);
		
	}
	
	@Test
	public void showAdvanceDeparture() {
		Employee emp = new Employee();
		Employee emp2 = new Employee();
		company.addEmployee(emp);
		company.addEmployee(emp2);
		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		emp2.setSchedule(ds);
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 8, 16);
		Pointing p1 = new Pointing(emp, t2);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 8, 40);
		Pointing p2 = new Pointing(emp2, t3);
		LocalDateTime t4 = LocalDateTime.of(2019, 3, 21, 17, 15);
		Pointing p3 = new Pointing(emp, t4);
		LocalDateTime t5 = LocalDateTime.of(2019, 3, 21, 18, 20);
		Pointing p4 = new Pointing(emp2, t5);
		
		company.addPointing(p1);
		company.addPointing(p2);
		company.addPointing(p3);
		company.addPointing(p4);
		
		List<Pointing> pointingAdvance = new ArrayList<Pointing>();
		pointingAdvance.add(p3);
		assertEquals(company.showAdvanceDeparture(),pointingAdvance);
	}
	
	@Test
	public void getAllEmpoyeesWithDepartment() {
		Department dep = new Department();
		company.addDepartment(dep);
		Employee emp = new Employee();
		emp.setDepartment(dep);
		company.addEmployee(emp);
		Employee emp2 = new Employee();
		emp2.setDepartment(dep);
		company.addEmployee(emp2);
		Employee emp3 = new Employee();
		company.addEmployee(emp3);
		
		List<Employee> list = company.getAllEmpoyeesWithDepartment(dep.getId());
		assertEquals(list.contains(emp), true);
		assertEquals(list.contains(emp2), true);
		assertEquals(list.size(), 2);
		
	}
	
	@Test
	public void getEmployee2() {
		Employee emp = new Employee();
		company.addEmployee(emp);
		assertEquals(company.getEmployee(emp.getId()),emp);
	}
	
	@Test
	public void getManager2() {
		Manager man = new Manager();
		company.addManager(man);
		assertEquals(company.getManager(man.getId()),man);
	}
	
	@Test
	public void getDepartment2() {
		Department dep = new Department();
		company.addDepartment(dep);
		assertEquals(company.getDepartment(dep.getId()),dep);
	}
	
	@Test
	public void searchPointing() {
		Employee emp = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		
		Department dep = new Department();
		company.addDepartment(dep);
		emp.setDepartment(dep);
		emp2.setDepartment(dep);
		
		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		emp2.setSchedule(ds);
		emp3.setSchedule(ds);
		
		company.addEmployee(emp);
		company.addEmployee(emp2);
		company.addEmployee(emp3);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 8, 16);
		Pointing p1 = new Pointing(emp, t2);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 8, 25);
		Pointing p2 = new Pointing(emp2, t3);
		LocalDateTime t4 = LocalDateTime.of(2019, 3, 21, 8, 35);
		Pointing p3 = new Pointing(emp3, t4);
		LocalDateTime t5 = LocalDateTime.of(2019, 3, 21, 18, 16);
		Pointing p4 = new Pointing(emp, t5);
		LocalDateTime t6 = LocalDateTime.of(2019, 3, 21, 18, 25);
		Pointing p5 = new Pointing(emp2, t6);
		LocalDateTime t7 = LocalDateTime.of(2019, 3, 21, 18, 35);
		Pointing p6 = new Pointing(emp3, t7);
		
		company.addPointing(p1);
		company.addPointing(p2);
		company.addPointing(p3);
		company.addPointing(p4);
		company.addPointing(p5);
		company.addPointing(p6);
		
		List<Pointing> pointings = new ArrayList<Pointing>();
		pointings.add(p1);
		pointings.add(p2);
		pointings.add(p4);
		pointings.add(p5);
		List<Pointing> pointingTest = company.searchPointing(null, null, null, dep);
		Collections.sort(pointings);
		Collections.sort(pointingTest);
		
		assertEquals(pointings, pointingTest);
	}
	
	@Test
	public void searchPointing2() {
		Employee emp = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		
		Department dep = new Department();
		company.addDepartment(dep);
		emp.setDepartment(dep);
		emp2.setDepartment(dep);
		
		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		emp2.setSchedule(ds);
		emp3.setSchedule(ds);
		
		company.addEmployee(emp);
		company.addEmployee(emp2);
		company.addEmployee(emp3);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 8, 16);
		Pointing p1 = new Pointing(emp, t2);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 8, 25);
		Pointing p2 = new Pointing(emp2, t3);
		LocalDateTime t4 = LocalDateTime.of(2019, 3, 21, 8, 35);
		Pointing p3 = new Pointing(emp3, t4);
		LocalDateTime t5 = LocalDateTime.of(2019, 3, 21, 18, 16);
		Pointing p4 = new Pointing(emp, t5);
		LocalDateTime t6 = LocalDateTime.of(2019, 3, 21, 18, 25);
		Pointing p5 = new Pointing(emp2, t6);
		LocalDateTime t7 = LocalDateTime.of(2019, 3, 21, 18, 35);
		Pointing p6 = new Pointing(emp3, t7);
		
		company.addPointing(p1);
		company.addPointing(p2);
		company.addPointing(p3);
		company.addPointing(p4);
		company.addPointing(p5);
		company.addPointing(p6);
		
		List<Pointing> pointings = new ArrayList<Pointing>();
		pointings.add(p2);
		pointings.add(p5);
		List<Pointing> pointingTest = company.searchPointing(null, null, emp2, null);
		
		assertEquals(pointings, pointingTest);
	}
	
	@Test
	public void searchPointing3() {
		Employee emp = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		
		Department dep = new Department();
		company.addDepartment(dep);
		emp.setDepartment(dep);
		emp2.setDepartment(dep);
		
		LocalTime t0 = LocalTime.of(8, 15);
		LocalTime t1 = LocalTime.of(18, 15);
		DaySchedule ds = new DaySchedule(t0,t1);
		emp.setSchedule(ds);
		emp2.setSchedule(ds);
		emp3.setSchedule(ds);
		
		company.addEmployee(emp);
		company.addEmployee(emp2);
		company.addEmployee(emp3);
		
		LocalDateTime t2 = LocalDateTime.of(2019, 3, 21, 8, 16);
		Pointing p1 = new Pointing(emp, t2);
		LocalDateTime t3 = LocalDateTime.of(2019, 3, 21, 8, 30);
		Pointing p2 = new Pointing(emp2, t3);
		LocalDateTime t4 = LocalDateTime.of(2019, 3, 21, 8, 30);
		Pointing p3 = new Pointing(emp3, t4);
		LocalDateTime t5 = LocalDateTime.of(2019, 3, 21, 18, 15);
		Pointing p4 = new Pointing(emp, t5);
		LocalDateTime t6 = LocalDateTime.of(2019, 3, 21, 18, 25);
		Pointing p5 = new Pointing(emp2, t6);
		LocalDateTime t7 = LocalDateTime.of(2019, 3, 21, 18, 35);
		Pointing p6 = new Pointing(emp3, t7);
		
		company.addPointing(p1);
		company.addPointing(p2);
		company.addPointing(p3);
		company.addPointing(p4);
		company.addPointing(p5);
		company.addPointing(p6);
		
		List<Pointing> pointings = new ArrayList<Pointing>();
		pointings.add(p2);
		pointings.add(p3);
		pointings.add(p4);
		List<Pointing> pointingTest = company.searchPointing(t3, t6, null, null);
		
		Collections.sort(pointings);
		Collections.sort(pointingTest);
		assertEquals(pointings, pointingTest);
	}
	
	@Test
	public void isManager() {
		Employee emp = new Employee();
		company.addEmployee(emp);
		Manager man = new Manager();
		company.addManager(man);
		assertEquals(company.isManager(man.getId()),true);
	}
	
	@Test
	public void isManager2() {
		Employee emp = new Employee();
		company.addEmployee(emp);
		Manager man = new Manager();
		company.addManager(man);
		assertEquals(company.isManager(emp.getId()),false);
	}
}