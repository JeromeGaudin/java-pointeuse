package test.model;

import org.junit.Before;
import org.junit.Test;

import src.model.DaySchedule;

import static org.junit.Assert.assertEquals;

import java.time.DateTimeException;
import java.time.LocalTime;

public class DayScheduleTest {
	private DaySchedule ds;
	
	@Before
	public void initilize() {
		LocalTime start = LocalTime.of(0, 5);
		LocalTime end = LocalTime.of(23, 55);
		
		ds = new DaySchedule(start, end);
	}
	
	@Test
	public void constructor_limiteDay() {
		assertEquals(ds.getStandardDepartureTime(),  LocalTime.of(0,0));
		assertEquals(ds.getStandardArrivalTime(),  LocalTime.of(0,0));
	}
	
	@Test
	public void  setStandardDepartureTime_normal() {
		LocalTime end = LocalTime.of(10, 8);
		
		ds.setStandardDepartureTime(end);
		assertEquals(ds.getStandardDepartureTime(),  LocalTime.of(10, 15));
	}
	
	@Test
	public void  setStandardArrivalTime_normal() {
		LocalTime start = LocalTime.of(18, 52);
		
		ds.setStandardArrivalTime(start);
		assertEquals(ds.getStandardArrivalTime(),  LocalTime.of(18, 45));
	}
	
	@Test (expected = DateTimeException.class)
	public void setStandardArrivalTime_exception() {
		LocalTime start = LocalTime.of(24, 52);
		
		ds.setStandardArrivalTime(start);
		assertEquals(ds.getStandardArrivalTime(),  LocalTime.of(18, 45));
	}	
}