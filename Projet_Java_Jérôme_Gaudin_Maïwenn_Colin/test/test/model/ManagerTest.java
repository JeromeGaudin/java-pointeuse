package test.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import src.model.Manager;

public class ManagerTest {
	private Manager manager;
	
	@Before
	public void initilize() {
		manager = new Manager();
	}
	
	@Test
	public void getManage() {
		assertEquals(manager.getManage(),false);
	}
	
	@Test
	public void setManage() {
		manager.setManage(true);
		assertEquals(manager.getManage(),true);
	}
	
	@Test
	public void default_constructor() {
		manager = new Manager();
		assertEquals(manager.getManage(),false);
	}
}