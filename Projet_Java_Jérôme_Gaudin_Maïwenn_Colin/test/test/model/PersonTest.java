package test.model;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import src.model.Person;

public class PersonTest {
	private static int idCountMin;
	private Person person;
	
	@BeforeClass 
	public static void idCount() {
		idCountMin = Person.getIdCount();
	}
	
	@Before
	public void initilize() {
		person = new Person("Alice", "Chinon");
	}
	
	@Test
	public void getFirstName() {
		assertEquals(person.getFirstName(),"Alice");
	}
	
	@Test
	public void getLastName() {
		assertEquals(person.getLastName(), "Chinon");
	}
	
	@Test
	public void setFirstName() {
		person.setFirstName("emilie");
		assertEquals(person.getFirstName(),"emilie");
	}
	
	@Test
	public void setLastName() {
		person.setLastName("Tours");
		assertEquals(person.getLastName(), "Tours");
	}
	
	@Test
	public void toStringTest() {
		assertEquals(person.toString(), person.getFirstName() + " " + person.getLastName());
	}
	
	@Test
	public void default_constructor() {
		person = new Person();
		assertEquals(person.getFirstName(),"");
		assertEquals(person.getLastName(),"");
	}
	
	@Test
	public void constructor() {
		person = new Person("Jean","Moulin");
		assertEquals(person.getFirstName(),"Jean");
		assertEquals(person.getLastName(),"Moulin");
	}
	
	@AfterClass
	public static void getId_getIdCount() {
		Person person = new Person();
		assertEquals(person.getId() - idCountMin, 9);
		assertEquals(person.getIdCount() - idCountMin, 10);
	}
}