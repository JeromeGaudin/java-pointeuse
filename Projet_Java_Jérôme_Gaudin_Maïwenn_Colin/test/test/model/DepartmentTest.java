package test.model;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Test;

import src.model.Department;

public class DepartmentTest {
	private Department dep;
	
	@Test
	public void default_constructor() {
		dep = new Department();
		assertEquals(dep.getName(),"");
	}
	
	@Test
	public void constructor() {
		dep = new Department("toto");
		assertEquals(dep.getName(),"toto");
	}
	
	@Test
	public void get_Name() {
		dep = new Department("toto");
		assertEquals(dep.getName(),"toto");
	}
	
	@Test
	public void setName() {
		dep = new Department("toto");
		dep.setName("tata");
		assertEquals(dep.getName(),"tata");
	}
	
	@Test
	public void toStringTest() {
		dep = new Department("toto");
		assertEquals(dep.toString(),"toto");
	}
	
	@AfterClass
	public static void getId() {
		Department dep = new Department();
		assertEquals(dep.getId(), dep.getIdCount()-1);
	}
}