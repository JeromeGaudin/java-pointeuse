Maïwenn Colin & Jérôme Gaudin

Ces deux applications correspondent au système d'une pointeuse que l'on peut trouver dans dans les usines par exmple. Une pointeuse permet d'enregisrer les arriver des travailleur et leurs sortie. Ce système fonctionne sur un réseau (ex le réseau de l'entreprise), ce qui permet d'avoir plusieurs pointeuses à différents endroits dans l'entreprise. L'application central permet de gérer les pointages : de voir les pointages, voir si des personnes sont en retard, si des personnes sont partie trop tôt. Elle permet aussi d'ajouter, de supprimer, de modifier des personnes dans le système. L'application pointeuse permet de simuler une pointeuse dans l'entreprise. Sur ces deux applications on peut bien sur régler l'ip et le port d'écoute.




Il faut absolument lancer la pointeuse (TimeCLock) avant l'application centrale (CentralApp).
Lorsqu'on synchronise les pointages il faut appuyer sur un des boutons de l'onglet "Pointage", pour les voir sur l'application centrale.
Si on change l'adresse IP ou le port, il faut redémarrer les deux applications.

Le package control contient toutes les classes nécessaires à l'échange de données entre l'application centrale et les pointeuses.
Le package lancher contient la classe principale de chaque application.
Le package model contient toutes les classes nécessaires pour le stockage de données.
Le package view contient toutes les classes permettant l'affichage de l'interface homme machine.
Le package view.component contient tous les composants utiles dans les vues, en tant qu'attributs de classes plus importantes.



La répartitions des classes dans les applications

Application centrale (CentralApp)
src.control
    Client 	
    ClientPerson 	 	 
    Server 	 	 
    ServerPointing
src.lancher 	 
    CentralApp
src.model
    Company 	 
    DaySchedule 	 
    Department 	 
    Employee 	 
    Manager 	 
    Person 	 
    Pointing 	 	 
    WorkingDay 	 
src.view
    AddEmployeeView 	 
    CentralAppSettings
    DepartmentView 	 
    EmployeeView 	 
    PointingView 	 	 
    WindowCentralApp 	 
src.view.component
    EmployeeTableModel 	 
    LocalDateInput 	 
    LocalTimeInput 	 
    PointingTableCellRenderer 	 
    PointingTableModel



La pointeuse (TimeCLock)
src.control
    Client 	
    ClientPointing	 	 
    Server 	 	 
    ServerPerson
src.lancher 	 
    TimeClock
src.model
    TimeClockData 
    Person 	 
    Pointing 	
src.view
    MainFrameTimeClock 	
    TimeClockSettings 
    WindowTimeClock


